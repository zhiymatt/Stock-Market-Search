<!DOCTYPE html>
<html>
	<head>
    	<title>Stock Search</title>
		<script src="https://code.highcharts.com/highcharts.src.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
        <style>
	        .centerContainer {
	    		display: flex;
	            justify-content: center;
	        }
			hr {
    			border: 1px solid rgb(210,210,210);
			}
	        .theForm {
    			border: 1px solid rgb(210,210,210);
				padding-left: 5px;
				padding-right: 5px;
	            width: 400px;
				text-align: left;
				background-color: rgb(245, 245, 245);
	        }
	        .theFormTitle {
				text-align: center;
				margin: 0px;
	            font-style: italic;
	        }
			.buttons {
				margin-top: 5px;
				margin-left: 190px;
			}
			input[type=button] {
				padding:5px 15px;
				background:#fff;
	    		border: 1px solid rgb(210,210,210);
				border-radius: 5px;
			}
			input[type=button]:active {
				background: rgb(70, 150, 255);
				background: -webkit-linear-gradient(rgb(70, 150, 255), rgb(10, 120, 235));
				background: -o-linear-gradient(rgb(70, 150, 255), rgb(10, 120, 235));
				background: -moz-linear-gradient(rgb(70, 150, 255), rgb(10, 120, 235));
				background: linear-gradient(rgb(70, 150, 255), rgb(10, 120, 235));
			}
			input[type=text] {
				border: 1px solid #ddd;
			}
			input[type=text]:focus {
				box-shadow: 0 0 5px rgba(143, 183, 222, 1);
				border: 1px solid rgba(143, 183, 222, 1);
			}
			#instruction {
				font-style: italic;
				margin-top: -15px;
			}
			#stockTable {
				margin-top: 10px;
			}
			#chartContainer {
				margin-top: 10px;
			}
			td, th {
    			border: 1px solid rgb(210,210,210);
			}
			table {
    			border: 2px solid rgb(210,210,210);
    			border-collapse: collapse;
				width: 1200px;
				font-family: Helvetica;
			}
			#stockTable th {
				text-align: left;
				background-color: rgb(243,243,243);
				width: 30%;
			}
			#stockTable td {
				text-align: center;
				background-color: rgb(251,251,251);
				width: 70%;
			}
			#stockChart {
				border: 2px solid rgb(210,210,210);
				width: 1200px;
				height: 550px;
			}
			.changePercentIndicator {
				height: 15px;
			}
			#grayArrow {
				height: 20px;
				width: 40px;
			}
			span.indicator {
				color: blue;
			}
			span.indicator:hover {
				color: black;
			}
			a {
				text-decoration: none;
				color: blue;
			}
			a:hover {
				text-decoration: none;
				color: black;
			}
			#hideOrShow {
				color: grey;
			}
			#stockNews button {
				outline:none;
				background-color: Transparent;
				border: none;
			}
			#newsContent {
				display: none;
				margin-top: 10px;
			}
			#newsContent tr {
				height: 40px;
			}
        </style>
		<script type="text/javascript">
			function resetEverything() {
				var infoSection = document.getElementById("infoSection");
				if (infoSection != null)
					document.body.removeChild(infoSection);
				var temp = document.getElementsByName("stockSymbol");
				temp[0].value = "";
			}
			function checkForm() {
				var what = document.getElementsByName('stockSymbol')[0].value;
				if (!(what.trim()))
					alert('Please enter a symbol');
				else
					document.getElementById('myQuery').submit();
			}
			function changeIndicator(what) {
				if (what == 'Price') {
					plotDefaultChart();
					return;
				}
				var url;
				switch(what) {
					case 'STOCH':
						url = "https://www.alphavantage.co/query?function=" + what + "&symbol=" + encodeURIComponent(stockSymbol) + "&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0&slowkmatype=1&slowdmatype=1";
						break;
					case 'BBANDS':
						url = "https://www.alphavantage.co/query?function=" + what + "&symbol=" + encodeURIComponent(stockSymbol) + "&interval=daily&time_period=5&series_type=close&apikey=S3HQD0NG2UABC1P0&nbdevup=3&nbdevdn=3";
						break;
					default:
						url = "https://www.alphavantage.co/query?function=" + what + "&symbol=" + encodeURIComponent(stockSymbol) + "&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0";
				}
				if (window.XMLHttpRequest) {
					xmlHttp = new XMLHttpRequest();
				} else {
					xmlHttp = new ActiveXObject("Microsoft.XMLHttp");
				}
				xmlHttp.open("GET", url, true);
				xmlHttp.overrideMimeType("application/json");
				xmlHttp.onreadystatechange = changingIndicator;
				xmlHttp.send(null);
				function extractSeries(jsonObj, what, properties) {
					var resultVal = [];
					for (var ii = 0; ii < properties.length; ii++)
						resultVal.push([]);
					var seriesData = jsonObj["Technical Analysis: " + what];
					if (Object.keys(seriesData).length == 0)
					    return resultVal;
					for (var dayPointer in dataDateY) {
						if (seriesData[dataDateY[dayPointer]] == undefined) {
							for (var ii = 0; ii < properties.length; ii++) {
								resultVal[ii].push(parseFloat(seriesData[jsonObj["Meta Data"]["3: Last Refreshed"]][properties[ii]]));
							}
						}
						else {
							for (var ii = 0; ii < properties.length; ii++) {
								resultVal[ii].push(parseFloat(seriesData[dataDateY[dayPointer]][properties[ii]]));
							}
						}
					}
					return resultVal;
				}
				function changingIndicator() {
					if (xmlHttp.readyState == 4) {
						if (xmlHttp.status == 200) {
							var doc = JSON.parse(xmlHttp.responseText);
							switch (what) {
								case 'SMA':
									resultVal = extractSeries(doc, what, ["SMA"]);
									theChart = Highcharts.chart('stockChart', {
										title: {
											text:  'Simple Moving Average (SMA)'
							        	},
							        	subtitle: {
											useHTML: true,
											text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
								        },
								        xAxis: [{
											tickLength: 4,
											categories: dataDate,
											tickInterval: 5,
											labels: {
												style: {
													fontSize: '8px'
												}
											}
								        }],
								        yAxis: [{
											title: {
												text: 'SMA'
											},
										}],
										legend: {
											enabled: true,
											layout: 'vertical',
											verticalAlign: 'middle',
											align:'right',
										},
								        exporting: {
											enabled: true
										},
								        series: [{
								            type: 'line',
								            name: stockSymbol,
								            data: resultVal[0],
											color: 'rgba(255, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 0, 0, 1)',
											}
								        }]
								    });
									break;
								case 'EMA':
									resultVal = extractSeries(doc, what, ["EMA"]);
									theChart = Highcharts.chart('stockChart', {
										title: {
											text:  'Exponential Moving Average (EMA)'
							        	},
							        	subtitle: {
											useHTML: true,
											text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
								        },
								        xAxis: [{
											tickLength: 4,
											categories: dataDate,
											tickInterval: 5,
											labels: {
												style: {
													fontSize: '8px'
												}
											}
								        }],
								        yAxis: [{
											title: {
												text: 'EMA'
											},
										}],
										legend: {
											enabled: true,
											layout: 'vertical',
											verticalAlign: 'middle',
											align:'right',
										},
								        exporting: {
											enabled: true
										},
								        series: [{
								            type: 'line',
								            name: stockSymbol,
								            data: resultVal[0],
											color: 'rgba(255, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 0, 0, 1)',
											}
								        }]
								    });
									break;
								case 'STOCH':
									resultVal = extractSeries(doc, what, ["SlowK", "SlowD"]);
									theChart = Highcharts.chart('stockChart', {
										title: {
											text:  'Stochastic Oscillator (STOCH)'
							        	},
							        	subtitle: {
											useHTML: true,
											text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
								        },
								        xAxis: [{
											tickLength: 4,
											categories: dataDate,
											tickInterval: 5,
											labels: {
												style: {
													fontSize: '8px'
												}
											}
								        }],
								        yAxis: [{
											title: {
												text: 'STOCH'
											},
										}],
										legend: {
											enabled: true,
											layout: 'vertical',
											verticalAlign: 'middle',
											align:'right',
										},
								        exporting: {
											enabled: true
										},
								        series: [{
								            type: 'line',
								            name: stockSymbol + ' SlowK',
								            data: resultVal[0],
											color: 'rgba(255, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 0, 0, 1)',
											}
								        }, {
								            type: 'line',
								            name: stockSymbol + ' SlowD',
								            data: resultVal[1],
											color: 'rgba(0, 0, 255, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(0, 0, 255, 1)',
											}
										}]
								    });
									break;
								case 'RSI':
									resultVal = extractSeries(doc, what, ["RSI"]);
									theChart = Highcharts.chart('stockChart', {
										title: {
											text:  'Relative Strength Index (RSI)'
							        	},
							        	subtitle: {
											useHTML: true,
											text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
								        },
								        xAxis: [{
											tickLength: 4,
											categories: dataDate,
											tickInterval: 5,
											labels: {
												style: {
													fontSize: '8px'
												}
											}
								        }],
								        yAxis: [{
											title: {
												text: 'RSI'
											},
										}],
										legend: {
											enabled: true,
											layout: 'vertical',
											verticalAlign: 'middle',
											align:'right',
										},
								        exporting: {
											enabled: true
										},
								        series: [{
								            type: 'line',
								            name: stockSymbol,
								            data: resultVal[0],
											color: 'rgba(255, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 0, 0, 1)',
											}
								        }]
								    });
									break;
								case 'ADX':
									resultVal = extractSeries(doc, what, ["ADX"]);
									theChart = Highcharts.chart('stockChart', {
										title: {
											text:  'Average Directional movement indeX (ADX)'
							        	},
							        	subtitle: {
											useHTML: true,
											text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
								        },
								        xAxis: [{
											tickLength: 4,
											categories: dataDate,
											tickInterval: 5,
											labels: {
												style: {
													fontSize: '8px'
												}
											}
								        }],
								        yAxis: [{
											title: {
												text: 'ADX'
											},
										}],
										legend: {
											enabled: true,
											layout: 'vertical',
											verticalAlign: 'middle',
											align:'right',
										},
								        exporting: {
											enabled: true
										},
								        series: [{
								            type: 'line',
								            name: stockSymbol,
								            data: resultVal[0],
											color: 'rgba(255, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 0, 0, 1)',
											}
								        }]
								    });
									break;
								case 'CCI':
									resultVal = extractSeries(doc, what, ["CCI"]);
									theChart = Highcharts.chart('stockChart', {
										title: {
											text:  'Commodity Channel Index (CCI)'
							        	},
							        	subtitle: {
											useHTML: true,
											text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
								        },
								        xAxis: [{
											tickLength: 4,
											categories: dataDate,
											tickInterval: 5,
											labels: {
												style: {
													fontSize: '8px'
												}
											}
								        }],
								        yAxis: [{
											title: {
												text: 'CCI'
											},
										}],
										legend: {
											enabled: true,
											layout: 'vertical',
											verticalAlign: 'middle',
											align:'right',
										},
								        exporting: {
											enabled: true
										},
								        series: [{
								            type: 'line',
								            name: stockSymbol,
								            data: resultVal[0],
											color: 'rgba(255, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 0, 0, 1)',
											}
								        }]
								    });
									break;
								case 'BBANDS':
									resultVal = extractSeries(doc, what, ["Real Upper Band", "Real Lower Band", "Real Middle Band"]);
									theChart = Highcharts.chart('stockChart', {
										title: {
											text:  'Bollinger Bands (BBANDS)'
							        	},
							        	subtitle: {
											useHTML: true,
											text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
								        },
								        xAxis: [{
											tickLength: 4,
											categories: dataDate,
											tickInterval: 5,
											labels: {
												style: {
													fontSize: '8px'
												}
											}
								        }],
								        yAxis: [{
											title: {
												text: 'BBANDS'
											},
										}],
										legend: {
											enabled: true,
											layout: 'vertical',
											verticalAlign: 'middle',
											align:'right',
										},
								        exporting: {
											enabled: true
										},
								        series: [{
								            type: 'line',
								            name: stockSymbol + " Real Middle Band",
								            data: resultVal[2],
											color: 'rgba(255, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 0, 0, 1)',
											}
								        }, {
											type: 'line',
											name: stockSymbol + " Real Upper Band",
											data: resultVal[0],
											color: 'rgba(0, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(0, 0, 0, 1)',
											}
								        }, {
											type: 'line',
											name: stockSymbol + " Real Lower Band",
											data: resultVal[1],
											color: 'rgba(0, 255, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(0, 255, 0, 1)',
											}
								        }]
								    });
									break;
								case 'MACD':
									resultVal = extractSeries(doc, what, ["MACD", "MACD_Signal", "MACD_Hist"]);
									theChart = Highcharts.chart('stockChart', {
										title: {
											text:  'Moving Average Convergence/Divergence (MACD)'
							        	},
							        	subtitle: {
											useHTML: true,
											text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
								        },
								        xAxis: [{
											tickLength: 4,
											categories: dataDate,
											tickInterval: 5,
											labels: {
												style: {
													fontSize: '8px'
												}
											}
								        }],
								        yAxis: [{
											title: {
												text: 'MACD'
											},
										}],
										legend: {
											enabled: true,
											layout: 'vertical',
											verticalAlign: 'middle',
											align:'right',
										},
								        exporting: {
											enabled: true
										},
								        series: [{
								            type: 'line',
								            name: stockSymbol + " MACD",
								            data: resultVal[0],
											color: 'rgba(255, 0, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 0, 0, 1)',
											}
								        }, {
								            type: 'line',
								            name: stockSymbol + " MACD_Hist",
								            data: resultVal[2],
											color: 'rgba(255, 128, 0, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(255, 128, 0, 1)',
											}
								        }, {
								            type: 'line',
								            name: stockSymbol + " MACD_Hist",
								            data: resultVal[2],
								            name: stockSymbol + " MACD_Signal",
								            data: resultVal[1],
											color: 'rgba(0, 0, 255, 0.5)',
											marker: {
												symbol: 'square',
												radius: 2,
												fillColor: 'rgba(0, 0, 255, 1)',
											}
								        }]
								    });
									break;
								default:
									//undefined
							}
						}
					}
				}
			}
		</script>
		<?php
			$greenArrowUp = "<img src='http://cs-server.usc.edu:45678/hw/hw6/images/Green_Arrow_Up.png' class='changePercentIndicator'>";
			$redArrowDown = "<img src='http://cs-server.usc.edu:45678/hw/hw6/images/Red_Arrow_Down.png' class='changePercentIndicator'>";
		?>
		<script type="text/javascript">
			grayArrowDown = "http://cs-server.usc.edu:45678/hw/hw6/images/Gray_Arrow_Down.png";
			grayArrowUp = "http://cs-server.usc.edu:45678/hw/hw6/images/Gray_Arrow_Up.png";
		</script>
	 </head>
	 <body>
		 <div class="centerContainer"><div class="theForm">
            <h1 class="theFormTitle">Stock Search</h1>
            <hr>
			<form id="myQuery" class="theFormBox" method="post">
				<span>Enter Stock Ticker Symbol:*</span>
				<input type="text" name="stockSymbol" value="<?php echo isset($_POST["stockSymbol"])? $_POST["stockSymbol"]: ""?>">
				<p class="buttons">
					<input type="button" name="search" value="Search" onclick="checkForm()">
					<input type="button" value="Clear" onclick="resetEverything()">
				</p>
				<div id="instruction"><p>* - Mandatory fields.<br><br><br></p></div>
			</form>
		</div></div>
		<?php
			if (isset($_POST["stockSymbol"])): {
					date_default_timezone_set('America/New_York');
					$symbol = trim($_POST["stockSymbol"]);
					/*
					 * stock information table
					 */
					$JSON = file_get_contents("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=".urlencode($symbol)."&apikey=S3HQD0NG2UABC1P0&outputsize=full");
					$JSONData = json_decode($JSON, TRUE);
					if (isset($JSONData["Error Message"])) {
						echo "\n\t\t<div id='infoSection'>";
						echo "\n\t\t\t<div id='stockTable' class='centerContainer'>";
						echo "\n\t\t\t\t<table><tr><th>Error</th><td>Error: NO record has been found, please enter a valid symbol</td><tr></table>";
						echo "</div></div>";
						return;
					}
					$seriesData = $JSONData["Time Series (Daily)"];
					$dayInterval = new DateInterval('P1D');
					$today = new DateTime('NOW');
					$lastDay = clone $today;
					while (!isset($seriesData[$lastDay->format('Y-m-d')])) {
						$lastDay->sub($dayInterval);
					}
					$previousDay = clone $lastDay;
					$previousDay->sub($dayInterval);
					while (!isset($seriesData[$previousDay->format('Y-m-d')])) {
						$previousDay->sub($dayInterval);
					}
					// extract price/volume
					$dataDate = array();
					$dataDateY = array();
					$dataPrice = array();
					$dataVolume = array();
					$dayPointer = clone $today;
					while ($dayPointer->diff($today)->m <= 5) {
						$tempDay = $dayPointer->format('Y-m-d');
						if (isset($seriesData[$tempDay])) {
							$dataDate[] = "\"".$dayPointer->format('m/d')."\"";
							$dataDateY[] = "\"".$dayPointer->format('Y-m-d')."\"";
							$dataPrice[] = $seriesData[$tempDay]["4. close"];
							$dataVolume[] = $seriesData[$tempDay]["5. volume"];
						}
						$dayPointer->sub($dayInterval);
					}
					$ttemp = (count($dataDate) - 1) % 5;
					$dataDate = array_slice(array_reverse($dataDate), $ttemp);
					$dataDateY = array_slice(array_reverse($dataDateY), $ttemp);
					$dataPrice = array_slice(array_reverse($dataPrice), $ttemp);
					$dataVolume = array_slice(array_reverse($dataVolume), $ttemp);
					echo "<script type='text/javascript'>";
					echo "\n\t\t\tvar stockSymbol = '".$symbol."';";
					echo "\n\t\t\tvar dataDate = [".implode(",", $dataDate)."];";
					echo "\n\t\t\tvar dataDateY = [".implode(",", $dataDateY)."];";
					echo "\n\t\t\tvar dataPrice = [".implode(",", $dataPrice)."];";
					echo "\n\t\t\tvar dataVolume = [".implode(",", $dataVolume)."];";
					echo "\n\t\t</script>";
					// html
					echo "\n\t\t<div id='infoSection'>";
					echo "\n\t\t\t<div id='stockTable' class='centerContainer'>";
					echo "\n\t\t\t\t<table><tr><th>Stock Ticker Symbol</th><td>".$JSONData["Meta Data"]["2. Symbol"]."</td><tr>";
					$lastClose = $seriesData[$lastDay->format('Y-m-d')]["4. close"];
					$previousClose = $seriesData[$previousDay->format('Y-m-d')]["4. close"];
					echo "<tr><th>Close</th><td>".$lastClose."</td><tr>";
					echo "<tr><th>Open</th><td>".$seriesData[$lastDay->format('Y-m-d')]["1. open"]."</td><tr>";
					echo "<tr><th>Previous Close</th><td>".$previousClose."</td><tr>";
					$change = $lastClose - $previousClose;
					if ($change > 0)
						$changePercentIndicator = $greenArrowUp;
					else if ($change < 0)
						$changePercentIndicator = $redArrowDown;
					else
						$changePercentIndicator = "";
					echo "<tr><th>Change</th><td>".number_format($change, 2, '.', '').$changePercentIndicator."</td><tr>";
					$changePercent = ($lastClose - $previousClose) / $previousClose * 100;
					if ($changePercent > 0)
						$changePercentIndicator = $greenArrowUp;
					else if ($changePercent < 0)
						$changePercentIndicator = $redArrowDown;
					else
						$changePercentIndicator = "";
					echo "<tr><th>Change Percent</th><td>".number_format($changePercent, 2, '.', '')."%".$changePercentIndicator."</td><tr>";
					echo "<tr><th>Day's Range</th><td>".$seriesData[$lastDay->format('Y-m-d')]["3. low"]."-".$seriesData[$lastDay->format('Y-m-d')]["2. high"]."</td><tr>";
					echo "<tr><th>Volume</th><td>".number_format($seriesData[$lastDay->format('Y-m-d')]["5. volume"])."</td><tr>";
					echo "<tr><th>Timestamp</th><td>".substr($JSONData["Meta Data"]["3. Last Refreshed"], 0, 10)."</td><tr>";
					echo "<tr><th>Indicators</th>";
					echo "<td><span class='indicator' onclick=\"changeIndicator('Price')\">Price&nbsp&nbsp&nbsp&nbsp</span>";
					echo "<span class='indicator' onclick=\"changeIndicator('SMA')\">SMA&nbsp&nbsp&nbsp&nbsp</span>";
					echo "<span class='indicator' onclick=\"changeIndicator('EMA')\">EMA&nbsp&nbsp&nbsp&nbsp</span>";
					echo "<span class='indicator' onclick=\"changeIndicator('STOCH')\">STOCH&nbsp&nbsp&nbsp&nbsp</span>";
					echo "<span class='indicator' onclick=\"changeIndicator('RSI')\">RSI&nbsp&nbsp&nbsp&nbsp</span>";
					echo "<span class='indicator' onclick=\"changeIndicator('ADX')\">ADX&nbsp&nbsp&nbsp&nbsp</span>";
					echo "<span class='indicator' onclick=\"changeIndicator('CCI')\">CCI&nbsp&nbsp&nbsp&nbsp</span>";
					echo "<span class='indicator' onclick=\"changeIndicator('BBANDS')\">BBANDS&nbsp&nbsp&nbsp&nbsp</span>";
					echo "<span class='indicator' onclick=\"changeIndicator('MACD')\">MACD</span></td>";
					echo "<tr>";
					echo "</table>\n\t\t\t</div>";
					/*
					 * stock price/volume/indicator chart
					*/
					echo "\n\t\t\t<div id='chartContainer' class='centerContainer'><div id='stockChart'></div></div>";
		?>
					<script type="text/javascript">
						priceMin = Math.min(...dataPrice);
						priceMax = Math.max(...dataPrice);
						function plotDefaultChart() {
						    theChart = Highcharts.chart('stockChart', {
						        title: {
									text:  'Stock Price (' + <?php echo substr($JSONData["Meta Data"]["3. Last Refreshed"], 5, 2); ?> + '/'
									+ <?php echo substr($JSONData["Meta Data"]["3. Last Refreshed"], 8, 2); ?> + '/'
									+ <?php echo substr($JSONData["Meta Data"]["3. Last Refreshed"], 0, 4); ?> + ')',
						        },
						        subtitle: {
									useHTML: true,
									text: '<a href=\"https://www.alphavantage.co/\" target=\"_blank\">Source: Alpha Vantage</a>',
						        },
						        xAxis: [{
									tickLength: 4,
									categories: dataDate,
									tickInterval: 5,
									labels: {
										style: {
											fontSize: '8px'
										}
									}
						        }],
						        yAxis: [{
									title: {
										text: 'Stock Price'
									},
									max: priceMax,
									min: priceMin - (priceMax - priceMin) / 2.5,
								} , {
									title: {
										text: 'Volume'
									},
									max: Math.max(...dataVolume) * 5,
									opposite: true
								}],
								legend: {
									enabled: true,
									layout: 'vertical',
									verticalAlign: 'middle',
									align:'right',
								},
						        plotOptions: {
						            area: {
										color: 'rgb(255, 0, 0)',
										lineColor: 'red',
										lineWidth: 1
						            },
									column: {
										color: 'white'
									}
						        },
								exporting: {
									enabled: true
								},
						        series: [{
									yAxis: 0,
						            type: 'area',
						            name: stockSymbol,
						            data: dataPrice,
									fillOpacity: 0.5,
									marker: {
										enabled: false
									},
									tooltip: {pointFormat: '<br/><span style="color:red">\u25CF</span>{series.name}: <b>{point.y:.2f}</b><br/>'}
						        }, {
									yAxis: 1,
									type: 'column',
									name: stockSymbol + ' Volume',
									data: dataVolume
								}]
						    });
						} // function plotDefaultChart
						plotDefaultChart();
					</script>
					<!--
					stock news
					-->
					<?php
						$newsData = [];
                        error_reporting(0);
						$xmlData = simplexml_load_file('https://seekingalpha.com/api/sa/combined/'.urlencode($symbol).'.xml');
                        error_reporting(E_ALL);
                        if ($xmlData !== false) {
                            $items = $xmlData->channel->item;
                            $ii = 0;
                            foreach ($items as $item) {
                                if ($ii >= 5)
                                    break;
                                if (strpos($item->link, "/article/") !== false) {
                                    $newsData[] = [$item->title, $item->pubDate, $item->link];
                                    $ii = $ii + 1;
                                }
                            }
                        }
					?>
					<div id='stockNews'>
						<div class='centerContainer'>
							<button onclick='changeShowNewsState()'>
								<p id='hideOrShow'>click to show stock news</p>
								<img id='grayArrow'>
							</button>
						</div>
						<div id='newsContent'>
							<div class='centerContainer'>
								<script type="text/javascript">
									var newsData = <?php print json_encode($newsData); ?>;
									document.write("<table>");
									for (ii = 0; ii < newsData.length; ii++) {
										document.write("<tr><td><a target='_blank' class='newsItemTitle' href='" + newsData[ii][2][0] + "'>" + newsData[ii][0][0] + "</a>&nbsp&nbsp&nbsp&nbsp&nbsp" +
											"</span><span class='newsItemContent'>Published Time: " + newsData[ii][1][0].substring(0, newsData[ii][1][0].length - 6) + "</span></td></tr>");
									}
									document.write("</table>");
								</script>
							</div>
						</div>
					</div>
					<script type="text/javascript">
						ifShowNews = false;
						document.getElementById('grayArrow').src = grayArrowDown;
						document.getElementById('newsContent').style.display = "none";
						function changeShowNewsState() {
							if (ifShowNews) {
								ifShowNews = false;
								document.getElementById('grayArrow').src = grayArrowDown;
								document.getElementById('newsContent').style.display = "none";
								document.getElementById('hideOrShow').innerHTML= "click to show stock news";
							}
							else {
								ifShowNews = true;
								document.getElementById('grayArrow').src = grayArrowUp;
								document.getElementById('newsContent').style.display = "block";
								document.getElementById('hideOrShow').innerHTML= "click to hide stock news";
							}
						}
					</script>
					<!-- -->
					</div>
		<?php
		}
		endif;
		?>
	</body>
</html>
