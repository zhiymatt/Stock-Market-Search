package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by miylo on 11/16/2017.
 */

public class NewsItemAdapter extends ArrayAdapter<NewsItem> {
    private static final String TAG = "NewsItemAdapter";

    private Context mcontext;
    private int mResource;

    public NewsItemAdapter(Context context, int resource, ArrayList<NewsItem> objects) {
        super(context, resource, objects);
        this.mcontext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String title = getItem(position).getNewsTitle();
        String author = getItem(position).getNewsAuthor();
        String date = getItem(position).getNewsDate();
        String link = getItem(position).getNewsLink();

        LayoutInflater inflater = LayoutInflater.from(mcontext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView titleField = (TextView) convertView.findViewById(R.id.titleField);
        TextView authorField = (TextView) convertView.findViewById(R.id.authorField);
        TextView dateField = (TextView) convertView.findViewById(R.id.dateField);

        titleField.setText(title);
        authorField.setText("Author: " + author);
        dateField.setText("Date: " + date);

        return convertView;
    }
}
