package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by miylo on 11/11/2017.
 */

public class NewsFragment extends Fragment {
    private static final String TAG = "newsFragment";

    private ProgressBar newsProgressBar;
    private TextView newsError;
    private ListView newsView;
    private View rootView;

    private String quoteSymbol;
    private String myDomain = "http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/";
    private RequestQueue queue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.news_fragment_result, container, false);
        newsView = rootView.findViewById(R.id.newsList);
        newsProgressBar = rootView.findViewById(R.id.newsProgressBar);
        newsError = rootView.findViewById(R.id.newsError);

        newsProgressBar.setVisibility(View.VISIBLE);
        newsError.setVisibility(View.GONE);

        quoteSymbol = ((ResultActivity)getContext()).quoteSymbol;
        queue = Volley.newRequestQueue(getContext());
        renderNews();

        return rootView;
    }

    private void renderNews() {
        // request and render news list
        String url = myDomain + "stock?symbol=" + quoteSymbol + "&type=NEWS";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {// Takes the response from the JSON request
                    @Override
                    public void onResponse(JSONArray response) {
                        renderNewsList(response);
                        newsProgressBar.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        newsProgressBar.setVisibility(View.GONE);
                        newsError.setVisibility(View.VISIBLE);
                        Log.d(TAG, "NEWS " + "JSON response/parse error");
                    }
                });
        jsonArrayRequest.setTag(TAG);
        queue.add(jsonArrayRequest);
    }

    private void renderNewsList(JSONArray newsArray) {
        ArrayList<NewsItem> newsList = new ArrayList<NewsItem>();
        for (int ii = 0; ii < newsArray.length(); ii++) {
            try {
                JSONArray temp = newsArray.getJSONArray(ii);
                newsList.add(new NewsItem(temp.getString(0), temp.getString(1), temp.getString(2), temp.getString(3)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        NewsItemAdapter adapter = new NewsItemAdapter(getContext(), R.layout.news_adapter, newsList);
        newsView.setAdapter(adapter);

        rootView.findViewById(R.id.newsList).setVisibility(View.VISIBLE); // newsList visible

        newsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String uriLink = ((NewsItem) newsView.getItemAtPosition(i)).getNewsLink();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriLink));
                startActivity(browserIntent);
            }
        });
    }
}
