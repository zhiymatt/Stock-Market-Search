package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by miylo on 11/16/2017.
 */

public class StockDetailItemAdapter extends ArrayAdapter<StockDetailItem> {
    private static final String TAG = "StockDetailItemAdapter";
    private Context mcontext;
    private int mResource;

    public StockDetailItemAdapter(Context context, int resource, ArrayList<StockDetailItem> objects) {
        super(context, resource, objects);
        this.mcontext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String head = getItem(position).getHead();
        String data = getItem(position).getData();
        int image = getItem(position).getImage();

        StockDetailItem stockDetailItem = new StockDetailItem(head, data, image);
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView detailHead = (TextView) convertView.findViewById(R.id.detailHead);
        TextViewWithImage detailData = (TextViewWithImage) convertView.findViewById(R.id.detailData);

        detailHead.setText(head);
        if (image != 0) {
            detailData.setText(data + "[img src=" + image + "/]");
        }
        else {
            detailData.setText(data);
        }

        return convertView;
    }
}
