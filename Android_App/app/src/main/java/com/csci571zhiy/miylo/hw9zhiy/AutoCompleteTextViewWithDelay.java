package com.csci571zhiy.miylo.hw9zhiy;

/**
 * Created by miylo on 11/19/2017.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

/**
 * Created by miylo on 11/19/2017.
 */

public class AutoCompleteTextViewWithDelay extends android.support.v7.widget.AppCompatAutoCompleteTextView {
    // enable delay and control progress bar
    private static final int MESSAGE_TEXT_CHANGED = 100;
    private int delay = 500;
    private Context mContext;

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            //ProgressBar progressBar = ((MainActivity)getContext()).findViewById(R.id.autoCompleteProgressBar);
            //progressBar.setVisibility(View.VISIBLE);
            AutoCompleteTextViewWithDelay.super.performFiltering((CharSequence) msg.obj, msg.arg1);
            //progressBar.setVisibility(View.GONE);
        }
    };

    public AutoCompleteTextViewWithDelay(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    protected void performFiltering(CharSequence text, int keyCode) {
        mHandler.removeMessages(MESSAGE_TEXT_CHANGED);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_TEXT_CHANGED, text), delay);
    }

    public void onFilterComplete(int count) {
        super.onFilterComplete(count);
    }
}