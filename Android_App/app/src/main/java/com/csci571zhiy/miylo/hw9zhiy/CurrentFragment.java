package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * Created by miylo on 11/11/2017.
 */

public class CurrentFragment extends Fragment {
    private static final String TAG = "currentFragment";

    private String symbol;

    private View rootView;
    private WebView chartView;
    private Spinner indicatorsSpinner;
    private int indicatorsSpinnerLastPosition  = -1;
    private Button indicatorsSelect;
    private ListView stockDetails;

    private String mCurrentChart;

    public ImageButton facebookButton;
    public ImageButton favoriteButton;

    // progress and error indicators
    private TextView tableError;
    private TextView chartError;
    private ProgressBar chartProgressBar;
    private ProgressBar tableProgressBar;

    // 0: fetching, 1: success, 2: fail
    public int ifADXChartReady;
    public int ifBBANDSChartReady;
    public int ifCCIChartReady;
    public int ifEMAChartReady;
    public int ifRSIChartReady;
    public int ifMACDChartReady;
    public int ifPriceChartReady;
    public int ifSMAChartReady;
    public int ifSTOCHChartReady;
    public int ifTableReady;


    private String myDomain = "http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/";
    private RequestQueue queue;

    // for adding stock to favorite list (in ResultActivity)
    public double stockPrice;
    public double stockChange;
    public double stockChangePercent;

    // from facebook SDK
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.current_fragment_result, container, false);
        this.chartView = rootView.findViewById(R.id.chartView);
        this.indicatorsSpinner = rootView.findViewById(R.id.indicatorsSpinner);
        // disable the one already selected
        this.indicatorsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (position != indicatorsSpinnerLastPosition) {
                    indicatorsSelect.setClickable(true);
                    indicatorsSelect.setTextColor(Color.BLACK);
                }
                else {
                    indicatorsSelect.setClickable(false);
                    indicatorsSelect.setTextColor(Color.GRAY);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) { }
        });
        this.indicatorsSelect = rootView.findViewById(R.id.indicatorsSelect);
        this.indicatorsSelect.setClickable(true);
        indicatorsSelect.setTextColor(Color.BLACK);
        this.stockDetails = rootView.findViewById(R.id.stockTable);

        queue = Volley.newRequestQueue(getContext());
        symbol = ((ResultActivity)getContext()).quoteSymbol;

        renderStockTable();
        renderCharts();

        // set facebook button and favorite button
        mCurrentChart = "none";
        facebookButton = (ImageButton) rootView.findViewById(R.id.facebookButton);
        favoriteButton = (ImageButton) rootView.findViewById(R.id.favoriteButton);
        if (((ResultActivity)getActivity()).getIfFavorite()) {
            favoriteButton.setImageResource(R.drawable.filled);
        }
        tableError = rootView.findViewById(R.id.tableError);
         chartError = rootView.findViewById(R.id.chartError);;

        ifADXChartReady = 0;
        ifBBANDSChartReady = 0;
        ifCCIChartReady = 0;
        ifRSIChartReady = 0;
        ifEMAChartReady = 0;
        ifMACDChartReady = 0;
        ifPriceChartReady = 0;
        ifSMAChartReady = 0;
        ifSTOCHChartReady = 0;
        ifTableReady = 0;
        tableError.setVisibility(View.GONE);
        chartError.setVisibility(View.INVISIBLE);
        favoriteButton.setClickable(false);
        facebookButton.setClickable(false);
        chartProgressBar = rootView.findViewById(R.id.chartProgressBar);
        tableProgressBar = rootView.findViewById(R.id.tableProgressBar);
        chartProgressBar.setVisibility(View.INVISIBLE);
        tableProgressBar.setVisibility(View.VISIBLE);

        Log.i(TAG, "currFrag oncreate");

        // facebook share toast
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) { Toast.makeText(getContext(), "posted", Toast.LENGTH_LONG).show(); }
            @Override
            public void onCancel() { Toast.makeText(getContext(), "canceled", Toast.LENGTH_LONG).show(); }
            @Override
            public void onError(FacebookException error) { Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show(); }
        });

        return rootView;
    }

    private void renderCharts() {
        chartView.getSettings().setJavaScriptEnabled(true);
        chartView.setWebContentsDebuggingEnabled(true);
        chartView.addJavascriptInterface(new JSInterface(), "JSInterface");
        String html = getHTML();
        chartView.loadData(html, "text/html", null);
    }

    private void changeChart(String chartType) {
        chartView.loadUrl("javascript:(function() {document.getElementById('PriceChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('SMAChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('EMAChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('STOCHChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('RSIChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('ADXChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('CCIChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('BBANDSChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('MACDChart').style.display='none';})()");
        chartView.loadUrl("javascript:(function() {document.getElementById('" + chartType + "Chart').style.display='block';})()");
    }

    public void selectIndicator(View view) {
        indicatorsSpinnerLastPosition = indicatorsSpinner.getSelectedItemPosition();
        indicatorsSelect.setClickable(false);
        indicatorsSelect.setTextColor(Color.GRAY);
        mCurrentChart = indicatorsSpinner.getSelectedItem().toString();
        updateChartStatus();
        changeChart(mCurrentChart);
    }

    public void renderStockTable() {
        String url = myDomain + "stock?symbol=" + symbol + "&type=DETAIL";
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {// Takes the response from the JSON request
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            double lastPrice = response.getDouble("lastPrice");
                            double change = response.getDouble("change");
                            double changePercent = response.getDouble("changePercent");
                            String timeStamp = response.getString("timeStamp");
                            double open = response.getDouble("open");
                            double previousClose = response.getDouble("previousClose");
                            String daysRange = response.getString("dayRange");
                            int volume = response.getInt("volume");

                            stockPrice = lastPrice;
                            stockChange = change;
                            stockChangePercent = changePercent;
                            ArrayList<StockDetailItem> stockDetailList = new ArrayList<StockDetailItem>();
                            stockDetailList.add(new StockDetailItem("Stock Symbol", symbol, 0));
                            stockDetailList.add(new StockDetailItem("Last Price", String.format("%.2f", lastPrice), 0));
                            if (change > 0) {
                                stockDetailList.add(new StockDetailItem("Change", String.format("%.2f", change) + " (" + String.format("%.2f", changePercent * 100) + "%)", R.drawable.up_icon));
                            }
                            else if (change < 0) {
                                stockDetailList.add(new StockDetailItem("Change", String.format("%.2f", change) + " (" + String.format("%.2f", changePercent * 100) + "%)", R.drawable.down_icon));
                            }
                            else {
                                stockDetailList.add(new StockDetailItem("Change", String.format("%.2f", change) + " (" + String.format("%.2f", changePercent * 100) + "%)", 0));
                            }
                            stockDetailList.add(new StockDetailItem("Timestamp", timeStamp, 0));
                            stockDetailList.add(new StockDetailItem("Open", String.format("%.2f", open), 0));
                            stockDetailList.add(new StockDetailItem("Close", String.format("%.2f", previousClose), 0));
                            stockDetailList.add(new StockDetailItem("Day's Range", daysRange, 0));
                            stockDetailList.add(new StockDetailItem("Volume", NumberFormat.getNumberInstance(Locale.US).format(volume), 0));
                            StockDetailItemAdapter adapter = new StockDetailItemAdapter(getContext(), R.layout.stock_detail_adapter, stockDetailList);
                            stockDetails.setAdapter(adapter);
                            rootView.findViewById(R.id.stockTable).setVisibility(View.VISIBLE); // stock table visible

                            ifTableReady = 1;
                            updateTableStatus();
                        } catch (JSONException e) {
                            ifTableReady = 2;
                            updateTableStatus();
                            Log.d(TAG, "TABLE " + "JSON parser error");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ifTableReady = 2;
                        updateTableStatus();
                        Log.d(TAG, "TABLE " + "JSON response error");
                    }
                });
        //jsObjRequest.setRetryPolicy(retryPolicy);
        jsObjRequest.setTag(TAG);
        queue.add(jsObjRequest);
    }

    public void updateChartStatus() {
        switch (mCurrentChart) {
            case ("Price"):
                if (ifPriceChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifPriceChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
            case ("ADX"):
                if (ifADXChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifADXChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
            case ("BBANDS"):
                if (ifBBANDSChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifBBANDSChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
            case ("CCI"):
                if (ifCCIChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifCCIChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
            case ("EMA"):
                if (ifEMAChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifEMAChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
            case ("MACD"):
                if (ifMACDChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifMACDChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
            case ("RSI"):
                if (ifRSIChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifRSIChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
            case ("SMA"):
                if (ifSMAChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifSMAChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
            case ("STOCH"):
                if (ifSTOCHChartReady == 1) {
                    facebookButton.setClickable(true);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else if (ifSTOCHChartReady == 2) {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.VISIBLE);
                    chartProgressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    facebookButton.setClickable(false);
                    chartError.setVisibility(View.INVISIBLE);
                    chartProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    public void updateTableStatus() {
        if (ifTableReady == 1) {
            tableProgressBar.setVisibility(View.GONE);
            tableError.setVisibility(View.GONE);
            favoriteButton.setClickable(true);
        } else if (ifTableReady == 2) {
            tableProgressBar.setVisibility(View.GONE);
            tableError.setVisibility(View.VISIBLE);
            favoriteButton.setClickable(false);
        }
    }

    private String getHTML() {
        return "<html>\n" +
                "<head>\n" +
                "    <script src=\"https://code.jquery.com/jquery-3.1.1.min.js\"></script>\n" +
                "    <script src=\"https://code.highcharts.com/stock/highstock.js\"></script>\n" +
                "    <script src=\"https://code.highcharts.com/stock/modules/exporting.js\"></script>\n" +
                "</head>\n" +
                "<body>\n" +
                "    <div id='PriceChart' style='display:none'></div>\n" +
                "    <div id='SMAChart' style='display:none'></div>\n" +
                "    <div id='EMAChart' style='display:none'></div>\n" +
                "    <div id='STOCHChart' style='display:none'></div>\n" +
                "    <div id='RSIChart' style='display:none'></div>\n" +
                "    <div id='ADXChart' style='display:none'></div>\n" +
                "    <div id='CCIChart' style='display:none'></div>\n" +
                "    <div id='BBANDSChart' style='display:none'></div>\n" +
                "    <div id='MACDChart' style='display:none'></div>\n" +
                "<script>\n" +
                "        symbol = '" + symbol + "'; chartConfig = {};\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=TIME', function (data) {\n" +
                "            priceMax = Math.max(...data['yY']['4. close']);\n" +
                "            volumeMax = Math.max(...data['yY']['5. volume']);\n" +
                "            ratio = 1;\n" +
                "            for (var ii = 0; ii < data['xX'].length; ii++) {\n" +
                "                temp = (data['yY']['4. close'][ii] * volumeMax) / ( data['yY']['5. volume'][ii] * priceMax);\n" +
                "                if (temp < 1) {\n" +
                "                    ratio = (temp < ratio) ? temp : ratio;\n" +
                "                }\n" +
                "            }" +
                "            Highcharts.chart('PriceChart', chartConfig['Price'] = {\n" +
                "                chart: {zoomType: 'x'},\n" +
                "                title: {\n" +
                "                    text: symbol + ' Stock Price and Volume',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                        'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 10,\n" +
                "                    labels: {style: {fontSize: '8px'}}\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'Stock Price'\n" +
                "                    },\n" +
                "                    max: priceMax\n" +
                "                }, {\n" +
                "                    title: {\n" +
                "                        text: 'Volume'\n" +
                "                    },\n" +
                "                    max: (volumeMax / ratio * 1.15),\n" +
                "                    opposite: true\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                plotOptions: {\n" +
                "                    area: {\n" +
                "                        color: '#2222fd',\n" +
                "                        lineColor: '#2222fd',\n" +
                "                        fillOpacity: 0.2,\n" +
                "                        lineWidth: 1,\n" +
                "                        tooltip: {pointFormat: '<br/><span style=\"color:#2222fd\">\\u25CF </span>{series.name}: <b>{point.y:.2f}</b><br/>'}\n" +
                "                    },\n" +
                "                    column: {\n" +
                "                        color: 'red'\n" +
                "                    },\n" +
                "                },\n" +
                "                exporting: {enabled: true},\n" +
                "                series: [{\n" +
                "                    yAxis: 0,\n" +
                "                    type: 'area',\n" +
                "                    name: 'Price',\n" +
                "                    lineWidth: 2,\n" +
                "                    data: data['yY']['4. close'],\n" +
                "                }, {\n" +
                "                    yAxis: 1,\n" +
                "                    type: 'column',\n" +
                "                    name: 'Volume',\n" +
                "                    data: data['yY']['5. volume'],\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('Price'); console.log('price chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('Price'); console.log('price chart err'); });\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=SMA', function (data) {\n" +
                "            Highcharts.chart('SMAChart', chartConfig['SMA'] = {\n" +
                "                chart: { zoomType: 'x'},\n" +
                "                title: {\n" +
                "                    text: 'Simple Moving Average (SMA)',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                        'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 5,\n" +
                "                    labels: { style: { fontSize: '8px' } }\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'SMA'\n" +
                "                    },\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                exporting: { enabled: true },\n" +
                "                series: [{\n" +
                "                    type: 'line',\n" +
                "                    name: symbol,\n" +
                "                    data: data['yY']['SMA']\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('SMA'); console.log('SMA chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('SMA'); console.log('SMA chart err'); });\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=EMA', function (data) {\n" +
                "            Highcharts.chart('EMAChart', chartConfig['EMA'] = {\n" +
                "                chart: { zoomType: 'x' },\n" +
                "                title: {\n" +
                "                    text: 'Exponential Moving Average (EMA)',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                    'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 5,\n" +
                "                    labels: { style: { fontSize: '8px' } }\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'EMA'\n" +
                "                    },\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                exporting: { enabled: true },\n" +
                "                series: [{\n" +
                "                    type: 'line',\n" +
                "                    name: symbol,\n" +
                "                    data: data['yY']['EMA'],\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('EMA'); console.log('EMA chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('EMA'); console.log('EMA chart err'); });\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=STOCH', function (data) {\n" +
                "            Highcharts.chart('STOCHChart', chartConfig['STOCH'] = {\n" +
                "                chart: { zoomType: 'x' },\n" +
                "                title: {\n" +
                "                    text: 'Stochastic Oscillator (STOCH)',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                    'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 5,\n" +
                "                    labels: { style: { fontSize: '8px' } }\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'STOCH'\n" +
                "                    },\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                exporting: { enabled: true },\n" +
                "                series: [{\n" +
                "                    type: 'line',\n" +
                "                    name: symbol + ' SlowK',\n" +
                "                    data: data['yY']['SlowK'],\n" +
                "                }, {\n" +
                "                    type: 'line',\n" +
                "                    name: symbol + ' SlowD',\n" +
                "                    data: data['yY']['SlowD'],\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('STOCH'); console.log('STOCH chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('STOCH'); console.log('STOCH chart err'); });\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=RSI', function (data) {\n" +
                "            Highcharts.chart('RSIChart', chartConfig['RSI'] = {\n" +
                "                chart: { zoomType: 'x' },\n" +
                "                title: {\n" +
                "                    text: 'Relative Strength Index (RSI)',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                    'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 5,\n" +
                "                    labels: { style: { fontSize: '8px' } }\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'RSI'\n" +
                "                    },\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                exporting: { enabled: true },\n" +
                "                series: [{\n" +
                "                    type: 'line',\n" +
                "                    name: symbol,\n" +
                "                    data: data['yY']['RSI'],\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('RSI'); console.log('RSI chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('RSI'); console.log('RSI chart err'); });\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=ADX', function (data) {\n" +
                "            Highcharts.chart('ADXChart', chartConfig['ADX'] = {\n" +
                "                chart: { zoomType: 'x' },\n" +
                "                title: {\n" +
                "                    text: 'Average Directional movement indeX (ADX)',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                    'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 5,\n" +
                "                    labels: { style: { fontSize: '8px' } }\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'ADX'\n" +
                "                    },\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                exporting: { enabled: true },\n" +
                "                series: [{\n" +
                "                    type: 'line',\n" +
                "                    name: symbol,\n" +
                "                    data: data['yY']['ADX'],\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('ADX'); console.log('ADX chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('ADX'); console.log('ADX chart err'); });\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=CCI', function (data) {\n" +
                "            Highcharts.chart('CCIChart', chartConfig['CCI'] = {\n" +
                "                chart: { zoomType: 'x' },\n" +
                "                title: {\n" +
                "                    text: 'Commodity Channel Index (CCI)',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                    'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 5,\n" +
                "                    labels: { style: { fontSize: '8px' } }\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'CCI'\n" +
                "                    },\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                exporting: { enabled: true },\n" +
                "                series: [{\n" +
                "                    type: 'line',\n" +
                "                    name: symbol,\n" +
                "                    data: data['yY']['CCI'],\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('CCI'); console.log('CCI chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('CCI'); console.log('CCI chart err'); });\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=BBANDS', function (data) {\n" +
                "            Highcharts.chart('BBANDSChart', chartConfig['BBANDS'] = {\n" +
                "                chart: { zoomType: 'x' },\n" +
                "                title: {\n" +
                "                    text: 'Bollinger Bands (BBANDS)',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                    'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 5,\n" +
                "                    labels: { style: { fontSize: '8px' } }\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'BBANDS'\n" +
                "                    },\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                exporting: { enabled: true },\n" +
                "                series: [{\n" +
                "                    type: 'line',\n" +
                "                    name: symbol + ' Real Upper Band',\n" +
                "                    data: data['yY']['Real Upper Band'],\n" +
                "                }, {\n" +
                "                    type: 'line',\n" +
                "                    name: symbol + ' Real Lower Band',\n" +
                "                    data: data['yY']['Real Lower Band'],\n" +
                "                }, {\n" +
                "                    type: 'line',\n" +
                "                    name: symbol + ' Real Middle Band',\n" +
                "                    data: data['yY']['Real Middle Band'],\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('BBANDS'); console.log('BBANDS chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('BBANDS'); console.log('BBANDS chart err'); });\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=MACD', function (data) {\n" +
                "            Highcharts.chart('MACDChart', chartConfig['MACD'] = {\n" +
                "                chart: { zoomType: 'x' },\n" +
                "                title: {\n" +
                "                    text: 'Moving Average Convergence/Divergence (MACD)',\n" +
                "                },\n" +
                "                subtitle: {\n" +
                "                    useHTML: true,\n" +
                "                    text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                    'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                },\n" +
                "                xAxis: [{\n" +
                "                    categories: data['xX'],\n" +
                "                    tickInterval: 5,\n" +
                "                    labels: { style: { fontSize: '8px' } }\n" +
                "                }],\n" +
                "                yAxis: [{\n" +
                "                    title: {\n" +
                "                        text: 'MACD'\n" +
                "                    },\n" +
                "                }],\n" +
                "                legend: {\n" +
                "                    enabled: true,\n" +
                "                    layout: 'horizontal',\n" +
                "                    align: 'center',\n" +
                "                    verticalAlign: 'bottom',\n" +
                "                },\n" +
                "                exporting: { enabled: true },\n" +
                "                series: [{\n" +
                "                    type: 'line',\n" +
                "                    name: symbol + ' MACD',\n" +
                "                    data: data['yY']['MACD'],\n" +
                "                }, {\n" +
                "                    type: 'line',\n" +
                "                    name: symbol + ' MACD_Signal',\n" +
                "                    data: data['yY']['MACD_Signal'],\n" +
                "                }, {\n" +
                "                    type: 'line',\n" +
                "                    name: symbol + ' MACD_Hist',\n" +
                "                    data: data['yY']['MACD_Hist'],\n" +
                "                }]\n" +
                "            })\n" +
                "        })\n" +
                "        .done(function() { JSInterface.setChartReady('MACD'); console.log('MACD chart done'); })\n" +
                "        .fail(function() { JSInterface.setChartError('MACD'); console.log('MACD chart err'); });\n" +
                "        </script>\n" +
                "</body>\n" +
                "</html>";
    }


    public class JSInterface {
        @JavascriptInterface
        public void setChartReady(final String chartType) {
            getActivity().runOnUiThread(new Runnable(){
                public void run(){
                    if (chartType.equals("Price")) {
                        ifPriceChartReady = 1;
                    }
                    else if (chartType.equals("SMA")) {
                        ifSMAChartReady = 1;
                    }
                    else if (chartType.equals("EMA")) {
                        ifEMAChartReady = 1;
                    }
                    else if (chartType.equals("STOCH")) {
                        ifSTOCHChartReady = 1;
                    }
                    else if (chartType.equals("RSI")) {
                        ifRSIChartReady = 1;
                    }
                    else if (chartType.equals("ADX")) {
                        ifADXChartReady = 1;
                    }
                    else if (chartType.equals("CCI")) {
                        ifCCIChartReady = 1;
                    }
                    else if (chartType.equals("BBANDS")) {
                        ifBBANDSChartReady = 1;
                    }
                    else if (chartType.equals("MACD")) {
                        ifMACDChartReady = 1;
                    }
                    else {}
                    updateChartStatus();
                }
            });
        }
        @JavascriptInterface
        public void setChartError(final String chartType) {
            getActivity().runOnUiThread(new Runnable(){
                public void run(){
                    if (chartType.equals("Price")) {
                        ifPriceChartReady = 2;
                    }
                    else if (chartType.equals("SMA")) {
                        ifSMAChartReady = 2;
                    }
                    else if (chartType.equals("EMA")) {
                        ifEMAChartReady = 2;
                    }
                    else if (chartType.equals("STOCH")) {
                        ifSTOCHChartReady = 2;
                    }
                    else if (chartType.equals("RSI")) {
                        ifRSIChartReady = 2;
                    }
                    else if (chartType.equals("ADX")) {
                        ifADXChartReady = 2;
                    }
                    else if (chartType.equals("CCI")) {
                        ifCCIChartReady = 2;
                    }
                    else if (chartType.equals("BBANDS")) {
                        ifBBANDSChartReady = 2;
                    }
                    else if (chartType.equals("MACD")) {
                        ifMACDChartReady = 2;
                    }
                    else {}
                    updateChartStatus();
                }
            });
        }
    }


    public void shareFacebook(View view) {
        chartView.evaluateJavascript("(function() { return chartConfig['" + mCurrentChart + "']; })();", new ValueCallback<String>() {
            @Override
            public void onReceiveValue(final String config) {
                Log.i(TAG, "shareFacebook called");
                String postUrl = myDomain + "export";
                StringRequest stringRequest = new StringRequest(Request.Method.POST, postUrl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                if (ShareDialog.canShow(ShareLinkContent.class)) {
                                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                            .setContentUrl(Uri.parse("http://export.highcharts.com/" + response))
                                            .build();
                                    shareDialog.show(linkContent);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.i(TAG, "export response error");
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("filename", "asdf");
                        params.put("options", config);
                        params.put("type", "image/png");
                        params.put("async", "true");
                        return params;
                    }
                };
                stringRequest.setTag(TAG);
                queue.add(stringRequest);
            }
        });
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
