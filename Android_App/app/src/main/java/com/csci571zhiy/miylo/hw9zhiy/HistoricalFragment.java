package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.File;

/**
 * Created by miylo on 11/11/2017.
 */

public class HistoricalFragment extends Fragment {
    private static final String TAG = "historicalFragment";

    private WebView histChartView;
    private TextView histChartError;
    private ProgressBar histChartProgressBar;

    private String symbol;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.historical_fragment_result, container, false);
        histChartView = rootView.findViewById(R.id.histChartView);
        histChartError = rootView.findViewById(R.id.histChartError);
        histChartProgressBar = rootView.findViewById(R.id.histChartProgressBar);

        histChartProgressBar.setVisibility(View.VISIBLE);
        histChartError.setVisibility(View.GONE);

        symbol = ((ResultActivity)getContext()).quoteSymbol;

        renderHistoricalChart();

        Log.i(TAG, "histFrag oncreate");
        return rootView;
    }

    private void renderHistoricalChart() {
        histChartView.getSettings().setJavaScriptEnabled(true);
        histChartView.setWebContentsDebuggingEnabled(true);
        histChartView.addJavascriptInterface(new JSInterface(), "JSInterface");
        String html = getHTML();
        histChartView.loadData(html, "text/html", null);
    }

    private String getHTML(){
        return "<html>\n" +
                "    <head>\n" +
                "        <script src=\"https://code.jquery.com/jquery-3.1.1.min.js\"></script>\n" +
                "        <script src=\"https://code.highcharts.com/stock/highstock.js\"></script>\n" +
                "        <script src=\"https://code.highcharts.com/stock/modules/exporting.js\"></script>\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <div id='HistChart' style='display:block'></div>\n" +
                "        <script>\n" +
                "        function setHistoricalChartReady(){}" +
                "        symbol = '" + symbol + "';\n" +
                "        $.getJSON('http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/stock?symbol=' + symbol + '&type=HISTORICAL', function (data) {\n" +
                "            Highcharts.stockChart('HistChart', {\n" +
                "                    chart: {},\n" +
                "                    title: {\n" +
                "                        text: symbol + ' Stock Value',\n" +
                "                    },\n" +
                "                    subtitle: {\n" +
                "                        useHTML: true,\n" +
                "                        text: '<a style=\\\"text-decoration: none\\\" href=\\\"https://www.alphavantage.co/\\\" ' +\n" +
                "                        'target=\\\"_blank\\\">Source: Alpha Vantage</a>',\n" +
                "                    },\n" +
                "                    yAxis: {\n" +
                "                        title: {\n" +
                "                            text: 'Stock Value'\n" +
                "                        }\n" +
                "                    },\n" +
                "                    rangeSelector: {\n" +
                "                        selected: 0,\n" +
                "                        buttons: [{\n" +
                "                            type: 'month',\n" +
                "                            count: 1,\n" +
                "                            text: '1m'\n" +
                "                        }, {\n" +
                "                            type: 'month',\n" +
                "                            count: 3,\n" +
                "                            text: '3m'\n" +
                "                        }, {\n" +
                "                            type: 'month',\n" +
                "                            count: 6,\n" +
                "                            text: '6m'\n" +
                "                        }, {\n" +
                "                            type: 'year',\n" +
                "                            count: 1,\n" +
                "                            text: '1y'\n" +
                "                        }, {\n" +
                "                            type: 'all',\n" +
                "                            text: 'All'\n" +
                "                        }]\n" +
                "                    },\n" +
                "                    series: [{\n" +
                "                        name: symbol,\n" +
                "                        data: data,\n" +
                "                        type: 'area',\n" +
                "                        tooltip: {\n" +
                "                            valueDecimals: 2\n" +
                "                        }\n" +
                "                    }],\n" +
                "                    tooltip: {\n" +
                "                        split: false\n" +
                "                    },\n" +
                "            });" +
                "        })\n" +
                "        .done(function() { JSInterface.setHistoricalChartReady(); console.log('historical chart done'); })\n" +
                "        .fail(function() { JSInterface.setHistoricalChartError(); console.log('historical chart err'); })\n" +
                "        </script>\n" +
                "    </body>\n" +
                "</html>";
    }

    public class JSInterface {
        @JavascriptInterface
        public void setHistoricalChartReady() {
            getActivity().runOnUiThread(new Runnable(){
                public void run(){
                    getActivity().findViewById(R.id.histChartProgressBar).setVisibility(View.GONE);
                    getActivity().findViewById(R.id.histChartError).setVisibility(View.GONE);
                }
            });
        }
        @JavascriptInterface
        public void setHistoricalChartError() {
            getActivity().runOnUiThread(new Runnable(){
                public void run(){
                    getActivity().findViewById(R.id.histChartProgressBar).setVisibility(View.GONE);
                    getActivity().findViewById(R.id.histChartError).setVisibility(View.VISIBLE);
                }
            });
        }
    }
}
