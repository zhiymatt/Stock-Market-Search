package com.csci571zhiy.miylo.hw9zhiy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import com.facebook.login.LoginManager;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    public static final String QUOTE_MESSAGE = "com.csci571zhiy.miylo.hw9zhiy.QUOTEMESSAGE";
    private String currentSymbol;

    private Spinner sortTypeSpinner;
    private Spinner sortDirectionSpinner;

    private ListView mfavListView;
    private FavoriteList favoriteList;
    BroadcastReceiver favoriteListReceiver;

    private String mSortType;
    private String mSortDirection;

    private AutoCompleteTextViewWithDelay editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // facebook login
        List<String> permissionNeeds = Arrays.asList("publish_actions");
        LoginManager loginManager = LoginManager.getInstance();
        loginManager.logInWithPublishPermissions(this, permissionNeeds);

        // set sort spinners
        sortTypeSpinner = findViewById(R.id.sortTypeSpinner);
        sortDirectionSpinner = findViewById(R.id.sortDirectionSpinner);
        List<String> sortTypeList = Arrays.asList(getResources().getStringArray(R.array.sort_by_type));
        List<String> sortDirectionList = Arrays.asList(getResources().getStringArray(R.array.sort_direction_type));
        final ArrayAdapter<String> sortTypeAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, sortTypeList){
            @Override
            public boolean isEnabled(int position){
                if(position == 0 || position == sortTypeSpinner.getSelectedItemPosition()) { return false; }
                else { return true; }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0 || position == sortTypeSpinner.getSelectedItemPosition()){ tv.setTextColor(Color.GRAY); }
                else { tv.setTextColor(Color.BLACK); }
                return view;
            }
        };
        final ArrayAdapter<String> sortDirectionAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, sortDirectionList){
            @Override
            public boolean isEnabled(int position){
                if (position == 0 || position == sortDirectionSpinner.getSelectedItemPosition()) { return false; }
                else { return true; }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0 || position == sortDirectionSpinner.getSelectedItemPosition()) { tv.setTextColor(Color.GRAY); }
                else { tv.setTextColor(Color.BLACK); }
                return view;
            }
        };
        sortTypeAdapter.setDropDownViewResource(R.layout.spinner_item);
        sortDirectionAdapter.setDropDownViewResource(R.layout.spinner_item);
        sortTypeSpinner.setAdapter(sortTypeAdapter);
        sortDirectionSpinner.setAdapter(sortDirectionAdapter);
        sortTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                if(position > 0){
                    //Toast.makeText(getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_LONG).show();
                    mSortType = selectedItemText;
                    favoriteList.sortOnType(mSortType);
                    renderFavoriteList();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        sortDirectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                if(position > 0){
                    //Toast.makeText(getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_LONG).show();
                    mSortDirection = selectedItemText;
                    favoriteList.sortOnDirection(mSortDirection);
                    renderFavoriteList();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        // load favorite list
        mfavListView = findViewById(R.id.favList);
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Log.i(TAG, "mainactivity onCreate");
        editor.commit();
        favoriteList = new FavoriteList(sharedPref, this);
        mSortType = "Default";
        mSortDirection = "Ascending";
        favoriteList.sortOnType(mSortType);
        favoriteList.sortOnDirection(mSortDirection);
        renderFavoriteList();
        favoriteList.refresh();

        // favorite list change broadcast receiver
        favoriteListReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String infoStr = intent.getStringExtra("infoStr");
                if (favoriteList.ifStockIsFavorite(currentSymbol)) {
                    // remove
                    favoriteList.removeStockFromFavoriteList(currentSymbol);
                    favoriteList.sortOnType(mSortType);
                    favoriteList.sortOnDirection(mSortDirection);
                    renderFavoriteList();
                }
                else {
                    // add
                    favoriteList.addStockToFavoriteList(currentSymbol, infoStr);
                    favoriteList.sortOnType(mSortType);
                    favoriteList.sortOnDirection(mSortDirection);
                    renderFavoriteList();
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(favoriteListReceiver, new IntentFilter("addOrRemoveFavorite"));

        // set favorite list item onClick
        mfavListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                editText.setText(((FavoriteItem) mfavListView.getItemAtPosition(i)).getSymbol());
                quoteStock(view);
            }
        });
        // registerForContextMenu(mfavListView);
        mfavListView.setLongClickable(true);
        mfavListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                showPopup(view, ((FavoriteItem) mfavListView.getItemAtPosition(i)).getSymbol());
                return true;
            }
        });

        // set autocomplete textview
        editText = (AutoCompleteTextViewWithDelay) findViewById(R.id.autoCompleteTextView);
        editText.setThreshold(1);
        final AutocompleteAdapter autocompleteAdapter = new AutocompleteAdapter(this, R.layout.autocomplete_layout);
        editText.setAdapter(autocompleteAdapter);
        editText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String chosenStock = autocompleteAdapter.getItem(position);
                editText.setText(chosenStock);
                editText.setSelection(chosenStock.length());
            }
        });

        // set autoRefresh switch
        Switch autoRefreshSwitch = (Switch) findViewById(R.id.autoRefreshSwitch);
        autoRefreshSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    favoriteList.enableAutoRefreshFavoriteList();
                }
                else {
                    favoriteList.disableAutoRefreshFavoriteList();
                }
            }

        });
    }

    public void showPopup(View v, final String symbol) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_favorite_list, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case (R.id.donotRemove):
                        //Toast.makeText(MainActivity.this, "Selected No", Toast.LENGTH_LONG).show();
                        break;
                    case (R.id.doRemove):
                        favoriteList.removeStockFromFavoriteList(symbol);
                        favoriteList.sortOnType(mSortType);
                        favoriteList.sortOnDirection(mSortDirection);
                        renderFavoriteList();
                        //Toast.makeText(MainActivity.this, "Selected Yes", Toast.LENGTH_LONG).show();
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    public void quoteStock(View view) {
        // remove everything except symbol
        currentSymbol = editText.getText().toString().split(" - ")[0].trim();
        // check if invalid
        if (currentSymbol.equals("")) {
            Toast.makeText(MainActivity.this, "Please enter a stock name or symbol", Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(this, ResultActivity.class);
        String[] message = {currentSymbol, favoriteList.ifStockIsFavorite(currentSymbol)?"isFavorite":"isNotFavorite"};
        intent.putExtra(QUOTE_MESSAGE, message);
        startActivity(intent);
        editText.setText("");
    }

    public void renderFavoriteList() {
        mfavListView.setAdapter(new FavListAdapter(this, R.layout.favorite_list_adapter, favoriteList.getFavList()));
        Log.i(TAG, "renderFavoriteList");
    }

    public void refreshFavoriteList(View view) {
        favoriteList.refresh();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // hide keyboard
        if (getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }

        Log.i(TAG, "resuming");

        favoriteList.refresh();
    }

    public void clear(View view) {
        editText.setText("");
    }
}
