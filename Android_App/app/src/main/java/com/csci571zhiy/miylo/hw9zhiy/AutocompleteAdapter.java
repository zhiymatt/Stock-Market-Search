package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


/**
 * Created by miylo on 11/19/2017.
 */

public class AutocompleteAdapter extends ArrayAdapter implements Filterable {

    public static final String TAG = "AutocompleteAdapter";

    private int MAX_RESULTS = 5;

    private List<String> suggestionList;
    private RequestQueue queue;

    public AutocompleteAdapter(Context context, int resourceId) {
        super(context, resourceId);
        suggestionList = new ArrayList<String>();
        queue = Volley.newRequestQueue(getContext());
    }

    @Override
    public int getCount() {
        return suggestionList.size();
    }

    @Override
    public String getItem(int position) {
        return suggestionList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.autocomplete_layout, parent, false);
        String suggest = suggestionList.get(position);
        TextView autoCompleteItem = (TextView) view.findViewById(R.id.autocompleteItem);
        autoCompleteItem.setText(suggest);
        return view;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint != null){// && !constraint.toString().trim().equals("")){
                    try {
                        ((MainActivity)getContext()).runOnUiThread(
                                new Runnable() {
                                    public void run() {
                                        ProgressBar progressBar = ((MainActivity) getContext()).findViewById(R.id.autoCompleteProgressBar);
                                        progressBar.setVisibility(View.VISIBLE);
                                    }
                                });
                        suggestionList = getAutoComplete(constraint.toString().trim());
                        ((MainActivity)getContext()).runOnUiThread(
                                new Runnable() {
                                    public void run() {
                                        ProgressBar progressBar = ((MainActivity) getContext()).findViewById(R.id.autoCompleteProgressBar);
                                        progressBar.setVisibility(View.GONE);
                    }
                });
                    } catch (ExecutionException e) {
                        Toast.makeText(getContext(), "Fail to get autocomplete.", Toast.LENGTH_SHORT).show();
                    } catch (InterruptedException e) {
                        Toast.makeText(getContext(), "Fail to get autocomplete.", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        Toast.makeText(getContext(), "Fail to get autocomplete.", Toast.LENGTH_SHORT).show();
                    }
                    filterResults.values = suggestionList;
                    filterResults.count = suggestionList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0){
                    notifyDataSetChanged();
                }else{
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }


    private List<String> getAutoComplete(String symbol) throws ExecutionException, InterruptedException, JSONException {
        String url = null;
        try {
            url = "http://dev.markitondemand.com/MODApis/Api/v2/Lookup/json?input=" + URLEncoder.encode(symbol, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RequestFuture<JSONArray> future = RequestFuture.newFuture();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, future, future);
        jsonArrayRequest.setTag(TAG);
        queue.add(jsonArrayRequest);

        List<String> tempSuggestList = new ArrayList<String>();
        JSONArray response = future.get();
        for (int i = 0; i < Math.min(MAX_RESULTS, response.length()); i++) {
            JSONObject jsonObject = response.getJSONObject(i);
            tempSuggestList.add(jsonObject.getString("Symbol") + " - " + jsonObject.getString("Name") +
                    " (" + jsonObject.getString("Exchange") + ")");
        }
        return tempSuggestList;
    }
}