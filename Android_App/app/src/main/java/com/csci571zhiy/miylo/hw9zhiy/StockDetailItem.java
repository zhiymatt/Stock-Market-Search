package com.csci571zhiy.miylo.hw9zhiy;

import android.content.res.Resources;

/**
 * Created by miylo on 11/16/2017.
 */

public class StockDetailItem {
    private String head;
    private String data;
    private int image;

    public StockDetailItem(String head, String data, int image) {
        this.head = head;
        this.data = data;
        this.image = image;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
