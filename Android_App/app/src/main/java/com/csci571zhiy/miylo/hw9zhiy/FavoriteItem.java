package com.csci571zhiy.miylo.hw9zhiy;

/**
 * Created by miylo on 11/17/2017.
 */

public class FavoriteItem {

    public static final String TAG = "FavoriteItem";

    private String symbol;
    private int order;
    private double price;
    private double change;
    private double changePercent;

    public FavoriteItem(String symbol, int order, double price, double change, double changePercent) {
        this.symbol = symbol;
        this.order = order;
        this.price = price;
        this.change = change;
        this.changePercent = changePercent;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public double getChangePercent() {
        return changePercent;
    }

    public void setChangePercent(double changePercent) {
        this.changePercent = changePercent;
    }
}
