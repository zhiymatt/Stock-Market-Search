package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by miylo on 11/21/2017.
 */

public class TextViewWithImage extends android.support.v7.widget.AppCompatTextView {

    private static final String PATTERN = "\\Q[img src=\\E([0-9_]+?)\\Q/]\\E";

    public TextViewWithImage(Context context) {
        super(context);
    }

    public TextViewWithImage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextViewWithImage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        Spannable spannable = Spannable.Factory.getInstance().newSpannable(text);
        Pattern imgPattern = Pattern.compile(PATTERN);
        Matcher matcher = imgPattern.matcher(spannable);
        if (matcher.find()) {
            int imgId = Integer.parseInt(spannable.subSequence(matcher.start(1), matcher.end(1)).toString().trim());
            Drawable drawable = getContext().getResources().getDrawable(imgId);
            drawable.mutate();
            drawable.setBounds(0, 0, getLineHeight(), getLineHeight());
            ImageSpan imageSpan = new ImageSpan(drawable, ImageSpan.ALIGN_BOTTOM);
            spannable.setSpan(imageSpan, matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        super.setText(spannable, BufferType.SPANNABLE);
    }
}