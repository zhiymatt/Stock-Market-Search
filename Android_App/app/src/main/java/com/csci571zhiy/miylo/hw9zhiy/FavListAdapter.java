package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by miylo on 11/17/2017.
 */

public class FavListAdapter  extends ArrayAdapter<FavoriteItem> {
    private static final String TAG = "FavListAdapter";

    private Context mcontext;
    private int mResource;

    public FavListAdapter(Context context, int resource, ArrayList<FavoriteItem> objects) {
        super(context, resource, objects);
        this.mcontext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String symbol = getItem(position).getSymbol();
        double price = getItem(position).getPrice();
        double change = getItem(position).getChange();
        double changePercent = getItem(position).getChangePercent();

        LayoutInflater inflater = LayoutInflater.from(mcontext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView favoriteSymbol = (TextView) convertView.findViewById(R.id.favoriteSymbol);
        TextView favoritePrice = (TextView) convertView.findViewById(R.id.favoritePrice);
        TextView favoriteChange = (TextView) convertView.findViewById(R.id.favoriteChange);

        favoriteSymbol.setText(symbol);
        favoritePrice.setText(String.format("%.2f", price));
        favoriteChange.setText(String.format("%.2f", change) + "(" + String.format("%.2f", changePercent*100) + "%)");
        if (change > 0) {
            favoriteChange.setTextColor(Color.rgb(120, 180, 66)); // green
        }
        else if (change < 0) {
            favoriteChange.setTextColor(Color.rgb(200, 20, 20)); // red
        }

        return convertView;
    }
}
