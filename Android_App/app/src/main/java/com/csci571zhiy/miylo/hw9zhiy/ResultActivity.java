
package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private static final String TAG = "ResultAcitivy";

    ViewPager mViewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;

    public String quoteSymbol;
    public boolean ifFavorite;

    public static final String QUOTE_MESSAGE = "com.csci571zhiy.miylo.hw9zhiy.QUOTEMESSAGE";

    private CurrentFragment currentFragment;
    private HistoricalFragment historicalFragment;
    private NewsFragment newsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        // Get quoted stock symbol and see if it's in the favorite list
        Intent intent = getIntent();
        String[] message = intent.getStringArrayExtra(MainActivity.QUOTE_MESSAGE);
        quoteSymbol = message[0];
        ifFavorite = message[1].equals("isFavorite");

        // toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.quoteTextField);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false); // disable default title
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // enable backhome
        TextView stockSymbolText = toolbar.findViewById(R.id.stockSymbol);
        stockSymbolText.setText(quoteSymbol);

        // set up the three fagments
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        currentFragment = new CurrentFragment();
        historicalFragment = new HistoricalFragment();
        newsFragment = new NewsFragment();
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(3);
        setupViewPager(mViewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(currentFragment, "currentFragment");
        adapter.addFragment(historicalFragment, "historicalFragment");
        adapter.addFragment(newsFragment, "newsFragment");
        viewPager.setAdapter(adapter);
    }

    public void selectIndicator(View view) {
        currentFragment.selectIndicator(view);
    }

    public boolean getIfFavorite() {
        return ifFavorite;
    }

    public void addOrRemoveFavorite(View view) {
        if (ifFavorite) {
            currentFragment.favoriteButton.setImageResource(R.drawable.empty);
        }
        else {
            currentFragment.favoriteButton.setImageResource(R.drawable.filled);
        }
        ifFavorite = !ifFavorite;
        Intent intent = new Intent("addOrRemoveFavorite");
        intent.putExtra("infoStr", Double.toString(currentFragment.stockPrice) + "#" +
                Double.toString(currentFragment.stockChange) + "#" + Double.toString(currentFragment.stockChangePercent));
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void shareFacebook(View view) {
        currentFragment.shareFacebook(view);
    }
}