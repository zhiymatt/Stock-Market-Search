package com.csci571zhiy.miylo.hw9zhiy;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by miylo on 11/17/2017.
 */

public class FavoriteList {

    // SharedPref: 1. all the stock symbols, 2. one pref for each stock
    // "name", "order, price, change, changePercent"

    public static final String TAG = "FavoriteList";

    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;

    ArrayList<FavoriteItem> favList = new ArrayList<FavoriteItem>();

    private String mSortType;
    private String mSortDirection;

    private RequestQueue queue;
    private String myDomain;

    private Context mContext;

    private Handler autoRefreshHandler;
    private Runnable mRefreshOneTime;

    public FavoriteList(SharedPreferences sharedPref, Context context) {
        mContext = context;
        mSharedPref = sharedPref;
        mEditor = mSharedPref.edit();
        // load from SharedPref
        Map<String, ?> allPref = mSharedPref.getAll();
        Set<String> keySet = allPref.keySet();
        for(String str : keySet){
            String[] temp = ((String) allPref.get(str)).split("#");
            // symbol, order, price, change, changePercent
            favList.add(new FavoriteItem(temp[0], Integer.parseInt(temp[1]), Double.parseDouble(temp[2]),
                    Double.parseDouble(temp[3]), Double.parseDouble(temp[4])));
        }
        mSortType = "Default";
        mSortDirection = "Ascending";

        queue = Volley.newRequestQueue(context);
        myDomain = "http://hw9zhiy-env.us-east-2.elasticbeanstalk.com/";
    }

    public void refreshStockInfo() {
        // get refresh information of particular stock from server
        // edit SharedPref
    }

    public void addStockToFavoriteList(String symbol, String infoStr) {
        String[] temp = infoStr.split("#");
        favList.add(new FavoriteItem(symbol, favList.size(), Double.parseDouble(temp[0]),
                Double.parseDouble(temp[1]), Double.parseDouble(temp[2])));
        mEditor.putString(symbol, symbol + "#" + (favList.size() - 1) + "#" + infoStr);
        mEditor.commit();
    }

    public void removeStockFromFavoriteList(String symbol) {
        int theOrder = Integer.MAX_VALUE;
        for (FavoriteItem item : favList) {
            if (item.getSymbol().equals(symbol)) {
                favList.remove(item);
                theOrder = item.getOrder();
                break;
            }
        }
        for (FavoriteItem item : favList) {
            if (item.getOrder() > theOrder) {
                item.setOrder(item.getOrder() - 1);
            }
        }
        mEditor.remove(symbol);
        mEditor.commit();
    }

    public boolean ifStockIsFavorite(String symbol) {
        Map<String, ?> allPref = mSharedPref.getAll();
        Set<String> keySet = allPref.keySet();
        return keySet.contains(symbol);
    }

    public ArrayList<FavoriteItem> getFavList() {
        Log.i(TAG, "getFavList");
        for (FavoriteItem item : favList) {
            Log.i(TAG, item.getSymbol() + "#" + item.getOrder() + "#" + item.getPrice() + "#" + item.getChange());
        }
        return favList;
    }

    public void sortOnType(String sortType) {
        Collections.sort(favList, new Comparator<FavoriteItem>() {
            @Override
            public int compare(FavoriteItem o1, FavoriteItem o2) {
                return o1.getOrder() - o2.getOrder();
            }
        });
        switch (sortType) {
            case "Default":
                break;
            case "Symbol":
                Collections.sort(favList, new Comparator<FavoriteItem>() {
                    @Override
                    public int compare(FavoriteItem o1, FavoriteItem o2) {
                        return o1.getSymbol().toUpperCase().compareTo(o2.getSymbol().toUpperCase());
                    }
                });
                break;
            case "Price":
                Collections.sort(favList, new Comparator<FavoriteItem>() {
                    @Override
                    public int compare(FavoriteItem o1, FavoriteItem o2) {
                        if (o1.getPrice() > o2.getPrice()) {
                            return 1;
                        } else if (o1.getPrice() < o2.getPrice()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                });
                break;
            case "Change":
                Collections.sort(favList, new Comparator<FavoriteItem>() {
                    @Override
                    public int compare(FavoriteItem o1, FavoriteItem o2) {
                        if (o1.getChange() > o2.getChange()) {
                            return 1;
                        } else if (o1.getChange() < o2.getChange()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                });
                break;
            case "Change(%)":
                Collections.sort(favList, new Comparator<FavoriteItem>() {
                    @Override
                    public int compare(FavoriteItem o1, FavoriteItem o2) {
                        if (o1.getChangePercent() > o2.getChangePercent()) {
                            return 1;
                        } else if (o1.getChangePercent() < o2.getChangePercent()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                });
                break;
            default:
        }
        if (mSortDirection.equals("Descending")) {
            Collections.reverse(favList);
        }
        mSortType = sortType;
    }
    public void sortOnDirection(String sortDirection) {
        if (!sortDirection.equals(mSortDirection)) {
            Collections.reverse(favList);
        }
        mSortDirection = sortDirection;
    }

    public void refresh() {
        // update each stock, then sort again, then reRenderFavoriteList
        // ((MainActivity)mContext).favoriteListProgressBar.setVisibility(View.VISIBLE);
        final ProgressBar progressBar = ((MainActivity)mContext).findViewById(R.id.favoriteListProgressBar);
        progressBar.setVisibility(View.VISIBLE);
        if (queue != null) {
            queue.cancelAll(TAG);
        }
        if (favList.size() == 0) {
            progressBar.setVisibility(View.GONE);
            return;
        }
        final int[] total = {favList.size()};
        for (final FavoriteItem item : favList) {
            String url = myDomain + "stock?symbol=" + item.getSymbol() + "&type=REFRESH";
            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {// Takes the response from the JSON request
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.i(TAG, "REFRESH  " + "JSON response");
                                double lastPrice = response.getDouble("lastPrice");
                                double change = response.getDouble("change");
                                double changePercent = response.getDouble("changePercent");
                                String infoStr = Double.toString(lastPrice) + "#" + Double.toString(change) + "#" + Double.toString(changePercent);
                                mEditor.putString(item.getSymbol(), item.getSymbol() + "#" + item.getOrder() + "#" + infoStr);
                                favList.set(favList.indexOf(item), new FavoriteItem(item.getSymbol(), item.getOrder(), lastPrice, change, changePercent));
                                total[0] = total[0] - 1;
                                if (total[0] == 0) {
                                    mEditor.commit();
                                    sortOnType(mSortType);
                                    sortOnDirection(mSortDirection);
                                    ((MainActivity) mContext).renderFavoriteList();
                                    progressBar.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                total[0] = total[0] - 1;
                                if (total[0] == 0) {
                                    Toast.makeText(mContext, "Unable to update the favorite list.", Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.GONE);
                                }
                                Log.d(TAG, "refresh  " + "JSON parser error");
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            total[0] = total[0] - 1;
                            if (total[0] == 0) {
                                Toast.makeText(mContext, "Unable to update the favorite list.", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            }
                            Log.d(TAG, "refresh response error");
                        }
                    });
            jsObjRequest.setTag(TAG);
            queue.add(jsObjRequest);
        }
    }


    public void enableAutoRefreshFavoriteList() {
        final int delay = 5000;
        autoRefreshHandler = new Handler();
        mRefreshOneTime = new Runnable() {
            public void run() {
                refresh();
                autoRefreshHandler.postDelayed(this, delay);
            }
        };
        autoRefreshHandler.postDelayed(mRefreshOneTime, delay);
    }


    public void disableAutoRefreshFavoriteList() {
        if (autoRefreshHandler != null && mRefreshOneTime != null) {
            autoRefreshHandler.removeCallbacks(mRefreshOneTime);
        }
    }
}
