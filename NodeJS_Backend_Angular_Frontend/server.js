const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
var Promise = require("bluebird");
var request = require('request');
var requestPromise = require('request-promise');
var moment = require('moment-timezone');

const app = express();

const ifdebug = false;

// parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// connect angular and express
app.use(express.static(path.join(__dirname, 'dist')));

// main route
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'src', 'index.html'));
});

// stock route
var maxRetries = 3;
app.get('/stock', function(req, res) {
    function extractStockInfo(stockUrl, retries, lines) {
        var xX = [];
        var yY = {};
        return requestPromise(stockUrl).then(function (body) {
            // fail
            if (body.length < 150) {
                if (ifdebug) {
                    console.log(body);
                }
                throw 'AlphavantageError';
            }
            // succeed
            // initial stock type and corresponding data
            var stockInfoJSON = JSON.parse(body);
            // initial each line
            for (var ii = 0; ii < lines.length; ii++) {
                yY[lines[ii]] = [];
            }
            // get series data and necessary information
            if (req.query.type.toUpperCase() == 'TIME') {
                var series = stockInfoJSON['Time Series (Daily)'];
                var day = moment.tz('America/New_York');
                var ii = 0;
                var seriesLen = Object.keys(series).length;
                while (ii < seriesLen) {
                    var temp = day.format('YYYY-MM-DD');
                    if (series[temp] !== undefined) {
                        xX.unshift(temp);
                        for (var jj = 0; jj < lines.length; jj++) {
                            yY[lines[jj]].unshift(parseFloat(series[temp][lines[jj]]));
                        }
                        ii++;
                    }
                    day.subtract(1, 'days');
                }
                var lastRefreshed = stockInfoJSON['Meta Data']['3. Last Refreshed'];
                var tempY = yY['4. close'];
                var lastPrice = tempY[tempY.length - 1];
                var change = tempY[tempY.length - 1] - tempY[tempY.length - 2];
                var changePercent = change / tempY[tempY.length - 2];
                var timeStamp;
                if (lastRefreshed.length <= 10) {
                    // if something wrong with this stock
                    if (moment.tz("America/New_York") < moment.tz(lastRefreshed + ' 16:00:00', "America/New_York")) {
                        timeStamp = moment.tz("America/New_York").format('YYYY-MM-DD HH:mm:ss') + ' ' + moment.tz('America/New_York').zoneAbbr();
                    } else {
                        timeStamp = moment.tz(lastRefreshed + ' 16:00:00', "America/New_York").format('YYYY-MM-DD HH:mm:ss') + ' ' + moment.tz('America/New_York').zoneAbbr();
                    }
                } else {
                    timeStamp = lastRefreshed.slice(0, 19) + ' ' + moment.tz('America/New_York').zoneAbbr();
                }
                var open = series[xX[xX.length - 1]]['1. open'];
                var previousClose = (lastRefreshed.length <= 10) ? lastPrice : tempY[tempY.length - 2];
                var dayRange = parseFloat(series[xX[xX.length - 1]]['3. low']).toFixed(2) + ' - ' + parseFloat(series[xX[xX.length - 1]]['2. high']).toFixed(2);
                var volume = series[xX[xX.length - 1]]['5. volume'];
                var before6months = moment.tz(lastRefreshed.substring(0, 10), "America/New_York").subtract(6, 'months');
                var index6months;
                for (index6months = xX.length - 1; index6months >= 0; index6months--){
                    if (moment.tz(xX[index6months], "America/New_York") <= before6months) {
                        break;
                    }
                }
                index6months = index6months + 1;
                if (xX.length - index6months > 9) {
                    index6months = (xX.length - index6months + 9) % 10 + index6months;
                }
                return {xX: xX, yY: yY, lastPrice: parseFloat(lastPrice), change: change, changePercent: changePercent, timeStamp: timeStamp,
                    open: parseFloat(open), previousClose: parseFloat(previousClose), dayRange: dayRange, volume: parseInt(volume), index6months: index6months};
            }
            else if (req.query.type.toUpperCase() == 'REFRESH') {
                var tempY = stockInfoJSON['Time Series (Daily)'];
                var today = stockInfoJSON['Meta Data']['3. Last Refreshed'].substring(0, 10);
                var yesterday = moment.tz(today, "America/New_York").subtract(1, 'days');
                while (tempY[yesterday.format('YYYY-MM-DD')] == undefined) {
                    yesterday.subtract(1, 'days');
                }
                yesterday = yesterday.format('YYYY-MM-DD');
                var lastPrice = tempY[today]['4. close'];
                var change = tempY[today]['4. close'] - tempY[yesterday]['4. close'];
                var changePercent = change / tempY[yesterday]['4. close'];
                var volume = tempY[today]['5. volume'];
                return {lastPrice: parseFloat(lastPrice), change: change, changePercent:changePercent, volume:parseInt(volume)};
            }
            else {
                var series = stockInfoJSON['Technical Analysis: ' + req.query.type.toUpperCase()];
                var day = moment.tz('America/New_York');
                var ii = 0;
                var seriesLen = Object.keys(series).length;
                var lastRefreshed = stockInfoJSON['Meta Data']['3: Last Refreshed'];
                if (series[lastRefreshed] !== undefined) {
                    xX.unshift(lastRefreshed.substring(0, 10));
                    for (var jj = 0; jj < lines.length; jj++) {
                        yY[lines[jj]].unshift(parseFloat(series[lastRefreshed][lines[jj]]));
                    }
                    ii++;
                    day.subtract(1, 'days');
                }
                while (ii < seriesLen) {
                    var temp = day.format('YYYY-MM-DD');
                    if (series[temp] !== undefined) {
                        xX.unshift(temp);
                        for (var jj = 0; jj < lines.length; jj++) {
                            yY[lines[jj]].unshift(parseFloat(series[temp][lines[jj]]));
                        }
                        ii++;
                    }
                    day.subtract(1, 'days');
                }
                var before6months = moment.tz(lastRefreshed.substring(0, 10), "America/New_York").subtract(6, 'months');
                var index6months;
                for (index6months = xX.length - 1; index6months >= 0; index6months--){
                    if (moment.tz(xX[index6months], "America/New_York") <= before6months) {
                        break;
                    }
                }
                index6months = index6months + 1;
                if (xX.length - index6months > 4) {
                    index6months = (xX.length - index6months + 4) % 5 + index6months;
                }
                return {xX: xX, yY: yY, index6months: index6months};
            }
        }).catch(function(err) {
            if (ifdebug) {
                console.log('Alpha Vantage Error: ' + (maxRetries - retries + 1) + ', ' + stockUrl.slice(43, 46));
            }
            return (retries > 0)? extractStockInfo(stockUrl, retries - 1, lines): null;
        });
    }
    switch (req.query.type.toUpperCase()) {
        case 'TIME':
            stockUrl = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=' + req.query.symbol + '&apikey=S3HQD0NG2UABC1P0&outputsize=full';
            extractStockInfo(stockUrl, maxRetries, ['4. close', '5. volume']).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock price sent');
                    } else {
                        console.log('stock price error')
                    }
                }
            });
            break;
        case 'SMA':
            stockUrl = 'https://www.alphavantage.co/query?function=SMA&symbol=' + req.query.symbol + '&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0';
            extractStockInfo(stockUrl, maxRetries, ['SMA']).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock sma sent');
                    } else {
                        console.log('stock sma error')
                    }
                }
            });
            break;
        case 'EMA':
            stockUrl = 'https://www.alphavantage.co/query?function=EMA&symbol=' + req.query.symbol + '&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0';
            extractStockInfo(stockUrl, maxRetries, ['EMA']).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock ema sent');
                    } else {
                        console.log('stock ema error')
                    }
                }
            });
            break;
        case 'STOCH':
            stockUrl = 'https://www.alphavantage.co/query?function=STOCH&symbol=' + req.query.symbol + '&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0&slowkmatype=1&slowdmatype=1';
            extractStockInfo(stockUrl, maxRetries, ['SlowK', 'SlowD']).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock stoch sent');
                    } else {
                        console.log('stock stoch error')
                    }
                }
            });
            break;
        case 'RSI':
            stockUrl = 'https://www.alphavantage.co/query?function=RSI&symbol=' + req.query.symbol + '&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0';
            extractStockInfo(stockUrl, maxRetries, ['RSI']).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock rsi sent');
                    } else {
                        console.log('stock rsi error')
                    }
                }
            });
            break;
        case 'ADX':
            stockUrl = 'https://www.alphavantage.co/query?function=ADX&symbol=' + req.query.symbol + '&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0';
            extractStockInfo(stockUrl, maxRetries, ['ADX']).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock adx sent');
                    } else {
                        console.log('stock adx error')
                    }
                }
            });
            break;
        case 'CCI':
            stockUrl = 'https://www.alphavantage.co/query?function=CCI&symbol=' + req.query.symbol + '&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0';
            extractStockInfo(stockUrl, maxRetries, ['CCI']).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock cci sent');
                    } else {
                        console.log('stock cci error')
                    }
                }
            });
            break;
        case 'BBANDS':
            stockUrl = 'https://www.alphavantage.co/query?function=BBANDS&symbol=' + req.query.symbol + '&interval=daily&time_period=5&series_type=close&apikey=S3HQD0NG2UABC1P0&nbdevup=3&nbdevdn=3';
            extractStockInfo(stockUrl, maxRetries, ["Real Upper Band", "Real Lower Band", "Real Middle Band"]).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock bbands sent');
                    } else {
                        console.log('stock bbands error')
                    }
                }
            });
            break;
        case 'MACD':
            stockUrl = 'https://www.alphavantage.co/query?function=MACD&symbol=' + req.query.symbol + '&interval=daily&time_period=10&series_type=close&apikey=S3HQD0NG2UABC1P0';
            extractStockInfo(stockUrl, maxRetries, ["MACD", "MACD_Signal", "MACD_Hist"]).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock macd sent');
                    } else {
                        console.log('stock macd error')
                    }
                }
            });
            break;
        case 'REFRESH':
            stockUrl = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=' + req.query.symbol + '&apikey=S3HQD0NG2UABC1P0';
            extractStockInfo(stockUrl, maxRetries, []).then(function(result) {
                res.send(JSON.stringify(result));
                if (ifdebug) {
                    if (result != null) {
                        console.log('stock refresh sent');
                    } else {
                        console.log('stock refresh error')
                    }
                }
            });
            break;
        case 'NEWS':
            stockUrl = 'https://seekingalpha.com/api/sa/combined/' + req.query.symbol + '.xml';
            requestPromise(stockUrl).then(function (body) {
                var parseString = require('xml2js').parseString;
                parseString(body, function (err, result) {
                    var temp = result['rss']['channel'][0]['item'];
                    var news = [];
                    var ii = 0;
                    for (var jj = 0; jj < temp.length; jj++) {
                        if (ii >= 5) {
                            break;
                        }
                        if (temp[jj]['link'][0].slice(25, 32) == 'article') {
                            var tempTime = temp[jj]['pubDate'][0];
                            news.push([temp[jj]['title'][0], temp[jj]['sa:author_name'][0],
                                tempTime.slice(0, 25) + ' ' + moment.tz(tempTime, "America/New_York").zoneAbbr(), temp[jj]['link'][0]]);
                            ii = ii + 1;
                        }
                    }
                    res.send(JSON.stringify(news));
                    if (ifdebug) {
                        console.log('stock news sent');
                    }
                });
            }).catch(function(err) {
                if (ifdebug) {
                    console.log('seekingalpha Error: ???');
                }
                res.send(JSON.stringify(null));
            });
            break;
        default:
        // undefined
    }
});

// autocomplete route
app.get('/autocomplete', function(req, res) {
    //req.pipe(request({url: 'http://dev.markitondemand.com/MODApis/Api/v2/Lookup/json?input=' + req.query.symbol,},
    //    function(error, response, body){res.send(JSON.stringify(null));console.log('autocomplete error');return;}
    //    )).pipe(res);
    // req.pipe(request('http://dev.markitondemand.com/MODApis/Api/v2/Lookup/json?input=' + req.query.symbol)).pipe(res);
    request('http://dev.markitondemand.com/MODApis/Api/v2/Lookup/json?input=' + req.query.symbol, function (error, response, body) {
        if (error) {
            res.send(JSON.stringify(null));
            if (ifdebug) {
                console.log('autocomplete error');
            }
        }
        else {
            res.send(body);
            if (ifdebug) {
                console.log('autocomplete send');
            }
        }
    });
});

// export image
app.post('/export', function(req, res) {
    //req.pipe(request.post({url: 'http://export.highcharts.com/', form: req.body},
    //    function(error, response, body){res.status(200);res.send(JSON.stringify(null));console.log('export error');return;}
    //)).pipe(res);
    // req.pipe(request.post({ url: 'http://export.highcharts.com/', form: req.body}), {end: false}).pipe(res);
    request.post({url:'http://export.highcharts.com/', form: req.body}, function(error, response, body){
        if (error) {
            res.send(JSON.stringify(null));
            if (ifdebug) {
                console.log('export error');
            }
        }
        else {
            res.send(body);
            if (ifdebug) {
                console.log('export url send');
            }
        }
    });
});

// port
const port = process.env.PORT || '3000';
app.set('port', port);

// create server and listen
const server = http.createServer(app);
server.listen(port);
if (ifdebug) {
    console.log("Listening on port " + port);
}

