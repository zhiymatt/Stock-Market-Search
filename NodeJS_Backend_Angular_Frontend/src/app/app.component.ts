// import basic
import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import 'zone.js';
import 'reflect-metadata';

// import chart ploting
import { ChartModule } from 'angular2-highcharts';

// import auto refresh helper
import 'rxjs/add/observable/timer';
import {Observable} from 'rxjs/Observable';

export class AutocompleteOption {
    constructor(public name: string, public info: string) { }
}

export class FavItem {
    constructor(public order: number, public symbol: string, public price: number, public change: number,
                public changePercent: number, public volume: number) { }
}

// facebook sdk
declare var FB: any;

// jquery
declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})

export class AppComponent {
    // form var
    stockForm: FormGroup;
    ifInvalidSymbol: boolean;
    currentStock: string; // stock symbol
    // tabs
    tabs1: number; // 1, 2, 3
    tabs2: number; // 0, 1, 2, ..., 8
    // favorite list
    isCurrentStockFavorite: boolean;
    ifAutoRefresh: boolean;
    autoRefreshSubscription: any;
    autoRefreshObservable: any;
    // stock information
    stockTableInfo: any;
    stockDetail: Object;
    // chart
    chart: Object[];
    chartInstance: any[];
    hisChart: Object;
    // news
    news: any;
    // autocomplete
    autocompleteOptions: AutocompleteOption[];
    // favorite list
    favList: FavItem[];

    constructor(formBuilder: FormBuilder, private http: HttpClient) {
        // form control
        this.stockForm = formBuilder.group({stockSymbol: ''});
        this.ifInvalidSymbol = true;
        this.currentStock = '';
        // tabs control
        this.tabs1 = 1;
        this.tabs2 = 0;
        // favorite list control
        this.isCurrentStockFavorite = false;
        this.ifAutoRefresh = false;
        this.stockDetail = {};
        // resume favorite list from browser local storage
        this.getFavList();
        // chart init
        this.chartInstance = [];
        this.chart = [];
    }

    // clear all
    clearAll() {
        // clear validation error
        this.stockForm.markAsUntouched();
        this.ifInvalidSymbol = true;
        // clear text field
        $('#stockSymbol').val('');
        this.currentStock = '';
        this.stockForm.reset();
        this.getAutocomplete();
        // reset result area to the favorite list, isRightVisible=false is done in the html file
        this.tabs1 = 1;
        this.tabs2 = 0;
        $('#tabs1Id a[herf="#curSto"]').tab('show');
        $('#tabs2Id a[href="#price-chart"]').tab('show');
        /*
        // reset favorite list
        this.ifAutoRefresh = false;
        this.favList = [];
        $('#sortby').val('1');
        $('#order').val('1');
        if (this.autoRefreshSubscription !== undefined && this.autoRefreshSubscription !== null) {
            this.autoRefreshSubscription.unsubscribe();
        }
        */
        // clear quote result
        this.stockDetail = {};
        this.stockTableInfo = undefined;
        this.chartInstance = [];
        this.chart = [];
        this.hisChart = undefined;
        this.news = undefined;
    }

    // stock symbol validation
    checkSymbol() {
        this.ifInvalidSymbol = $('#stockSymbol').val().trim() === '';
    }

    // autocomplete component
    getAutocomplete() {
        this.http.get('/autocomplete?symbol=' + $('#stockSymbol').val().trim()).subscribe(data => {
            if (data == null) {
                // console.log('autocomplete error');
            } else {
                this.autocompleteOptions = [];
                for (let ii = 0; ii < data['length']; ii++) {
                    this.autocompleteOptions.push(new AutocompleteOption(data[ii]['Symbol'],
                        data[ii]['Symbol'] + ' - ' + data[ii]['Name'] + ' (' + data[ii]['Exchange'] + ')'));
                }
            }
        }, (err) => {
            // console.log('autocomplete error');
        });
    }

    numberWithCommas(x: number) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    // search stock information
    stockSearch() {
        // change tabs
        this.currentStock = $('#stockSymbol').val();
        this.tabs1 = 1;
        $('#tabs1Id a[herf="#curSto"]').tab('show');
        // $('#tabs2Id a[href="#price-chart"]').tab('show');
        // clear old stock information
        this.stockDetail = {};
        this.stockTableInfo = undefined;
        this.chartInstance = [];
        this.chart = [];
        this.news = undefined;
        this.hisChart = undefined;
        // console.log('searching ' + this.currentStock);
        // check if it's in favorite list
        this.isCurrentStockFavorite = false;
        for (let ii = 0; ii < this.favList.length; ii++) {
            if (this.favList[ii].symbol.toUpperCase() === this.currentStock.toUpperCase()) {
                this.isCurrentStockFavorite = true;
                break;
            }
        }
        this.http.get('/stock?symbol=' + this.currentStock + '&type=TIME').subscribe(data => {
            if (data == null) {
                this.stockTableInfo = null;
                this.chart[0] = null;
                this.hisChart = null;
                return;
            }
            const xX = [];
            const stockVal = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                // xaxis label
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
                // hischart data
                stockVal.push([new Date(data['xX'][ii]).getTime(), data['yY']['4. close'][ii]]);
            }
            this.stockTableInfo = data;
            this.stockDetail['TIME'] = data;
            const priceData6month = data['yY']['4. close'].slice(data['index6months']);
            const volumeData6month = data['yY']['5. volume'].slice(data['index6months']);
            // prevent volume value over price value
            const priceMax = Math.max(...priceData6month);
            const volumeMax = Math.max(...priceData6month);
            let ratio = 1;
            for (let ii = 0; ii < priceData6month.length; ii++) {
                const temp = (priceData6month[ii] * volumeMax) / ( volumeData6month[ii] * priceMax);
                if (temp < 1) {
                    ratio = (temp < ratio) ? temp : ratio;
                }
            }
            this.chart[0] = {
                chart: {zoomType: 'x'},
                title: {
                    text: this.currentStock + ' Stock Price and Volume',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                        'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 10,
                    labels: {style: {fontSize: '8px'}}
                }],
                yAxis: [{
                    title: {
                        text: 'Stock Price'
                    },
                    max: priceMax * 1.05
                }, {
                    title: {
                        text: 'Volume'
                    },
                    max: volumeMax / ratio * 1.15,
                    opposite: true
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                plotOptions: {
                    area: {
                        color: '#2222fd',
                        lineColor: '#2222fd',
                        fillOpacity: 0.2,
                        lineWidth: 1,
                        tooltip: {pointFormat: '<br/><span style="color:#2222fd">\u25CF </span>{series.name}: <b>{point.y:.2f}</b><br/>'}
                    },
                    column: {
                        color: 'red'
                    },
                },
                exporting: {enabled: true},
                series: [{
                    yAxis: 0,
                    type: 'area',
                    name: 'Price',
                    lineWidth: 2,
                    data: priceData6month,
                }, {
                    yAxis: 1,
                    type: 'column',
                    name: 'Volume',
                    data: volumeData6month,
                }]
            };
            this.hisChart = {
                chart: {},
                title: {
                    text: this.currentStock + ' Stock Value',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                    'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                yAxis: {
                    title: {
                        text: 'Stock Value'
                    }
                },
                rangeSelector: {
                    selected: 0,
                    buttons: [{
                        type: 'week',
                        count: 1,
                        text: '1w'
                    }, {
                        type: 'month',
                        count: 1,
                        text: '1m'
                    }, {
                        type: 'month',
                        count: 3,
                        text: '3m'
                    }, {
                        type: 'month',
                        count: 6,
                        text: '6m'
                    }, {
                        type: 'ytd',
                        text: 'YTD'
                    }, {
                        type: 'year',
                        count: 1,
                        text: '1y'
                    }, {
                        type: 'all',
                        text: 'All'
                    }]
                },
                series: [{
                    name: this.currentStock,
                    data: stockVal.slice(stockVal.length - 1000),
                    type: 'area',
                    tooltip: {
                        valueDecimals: 2
                    }
                }],
                tooltip: {
                    split: false
                },
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 768
                        },
                        chartOptions: {
                            chart: {
                                height: 600
                            },
                            rangeSelector: {
                                selected: 0,
                                inputEnabled: false,
                                buttons: [{
                                    type: 'month',
                                    count: 1,
                                    text: '1m'
                                }, {
                                    type: 'month',
                                    count: 3,
                                    text: '3m'
                                }, {
                                    type: 'month',
                                    count: 6,
                                    text: '6m'
                                }, {
                                    type: 'year',
                                    count: 1,
                                    text: '1y'
                                }, {
                                    type: 'all',
                                    text: 'All'
                                }]
                            },
                        }
                    }]
                }
            };
        }, (err) => {
            // console.log('price connect error');
            this.stockTableInfo = null;
            this.chart[0] = null;
            this.hisChart = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=SMA').subscribe(data => {
            if (data == null) {
                this.chart[1] = null;
                return;
            }
            const xX = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
            }
            this.stockDetail['SMA'] = data;
            this.chart[1] = {
                chart: { zoomType: 'x'},
                title: {
                    text: 'Simple Moving Average (SMA)',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                        'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 5,
                    labels: { style: { fontSize: '8px' } }
                }],
                yAxis: [{
                    title: {
                        text: 'SMA'
                    },
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                exporting: { enabled: true },
                series: [{
                    type: 'line',
                    name: this.currentStock,
                    data: data['yY']['SMA'].slice(data['index6months'])
                }]
            };
        }, (err) => {
            // console.log('sma connect error');
            this.chart[1] = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=EMA').subscribe(data => {
            if (data == null) {
                this.chart[2] = null;
                return;
            }
            const xX = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
            }
            this.stockDetail['EMA'] = data;
            this.chart[2] = {
                chart: { zoomType: 'x' },
                title: {
                    text: 'Exponential Moving Average (EMA)',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                    'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 5,
                    labels: { style: { fontSize: '8px' } }
                }],
                yAxis: [{
                    title: {
                        text: 'EMA'
                    },
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                exporting: { enabled: true },
                series: [{
                    type: 'line',
                    name: this.currentStock,
                    data: data['yY']['EMA'].slice(data['index6months']),
                }]
            };
        }, (err) => {
            // console.log('ema connect error');
            this.chart[2] = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=STOCH').subscribe(data => {
            if (data == null) {
                this.chart[3] = null;
                return;
            }
            const xX = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
            }
            this.stockDetail['STOCH'] = data;
            this.chart[3] = {
                chart: { zoomType: 'x' },
                title: {
                    text: 'Stochastic Oscillator (STOCH)',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                    'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 5,
                    labels: { style: { fontSize: '8px' } }
                }],
                yAxis: [{
                    title: {
                        text: 'STOCH'
                    },
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                exporting: { enabled: true },
                series: [{
                    type: 'line',
                    name: this.currentStock + ' SlowK',
                    data: data['yY']['SlowK'].slice(data['index6months']),
                }, {
                    type: 'line',
                    name: this.currentStock + ' SlowD',
                    data: data['yY']['SlowD'].slice(data['index6months']),
                }]
            };
        }, (err) => {
            // console.log('stoch connect error');
            this.chart[3] = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=RSI').subscribe(data => {
            if (data == null) {
                this.chart[4] = null;
                return;
            }
            const xX = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
            }
            this.stockDetail['RSI'] = data;
            this.chart[4] = {
                chart: { zoomType: 'x' },
                title: {
                    text: 'Relative Strength Index (RSI)',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                    'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 5,
                    labels: { style: { fontSize: '8px' } }
                }],
                yAxis: [{
                    title: {
                        text: 'RSI'
                    },
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                exporting: { enabled: true },
                series: [{
                    type: 'line',
                    name: this.currentStock,
                    data: data['yY']['RSI'].slice(data['index6months']),
                }]
            };
        }, (err) => {
            // console.log('rsi connect error');
            this.chart[4] = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=ADX').subscribe(data => {
            if (data == null) {
                this.chart[5] = null;
                return;
            }
            const xX = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
            }
            this.stockDetail['ADX'] = data;
            this.chart[5] = {
                chart: { zoomType: 'x' },
                title: {
                    text: 'Average Directional movement indeX (ADX)',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                    'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 5,
                    labels: { style: { fontSize: '8px' } }
                }],
                yAxis: [{
                    title: {
                        text: 'ADX'
                    },
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                exporting: { enabled: true },
                series: [{
                    type: 'line',
                    name: this.currentStock,
                    data: data['yY']['ADX'].slice(data['index6months']),
                }]
            };
        }, (err) => {
            // console.log('adx connect error');
            this.chart[5] = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=CCI').subscribe(data => {
            if (data == null) {
                this.chart[6] = null;
                return;
            }
            const xX = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
            }
            this.stockDetail['CCI'] = data;
            this.chart[6] = {
                chart: { zoomType: 'x' },
                title: {
                    text: 'Commodity Channel Index (CCI)',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                    'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 5,
                    labels: { style: { fontSize: '8px' } }
                }],
                yAxis: [{
                    title: {
                        text: 'CCI'
                    },
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                exporting: { enabled: true },
                series: [{
                    type: 'line',
                    name: this.currentStock,
                    data: data['yY']['CCI'].slice(data['index6months']),
                }]
            };
        }, (err) => {
            // console.log('cci connect error');
            this.chart[6] = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=BBANDS').subscribe(data => {
            if (data == null) {
                this.chart[7] = null;
                return;
            }
            const xX = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
            }
            this.stockDetail['BBANDS'] = data;
            this.chart[7] = {
                chart: { zoomType: 'x' },
                title: {
                    text: 'Bollinger Bands (BBANDS)',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                    'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 5,
                    labels: { style: { fontSize: '8px' } }
                }],
                yAxis: [{
                    title: {
                        text: 'BBANDS'
                    },
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                exporting: { enabled: true },
                series: [{
                    type: 'line',
                    name: this.currentStock + ' Real Upper Band',
                    data: data['yY']['Real Upper Band'].slice(data['index6months']),
                }, {
                    type: 'line',
                    name: this.currentStock + ' Real Lower Band',
                    data: data['yY']['Real Lower Band'].slice(data['index6months']),
                }, {
                    type: 'line',
                    name: this.currentStock + ' Real Middle Band',
                    data: data['yY']['Real Middle Band'].slice(data['index6months']),
                }]
            };
        }, (err) => {
            // console.log('bbands connect error');
            this.chart[7] = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=MACD').subscribe(data => {
            if (data == null) {
                this.chart[8] = null;
                return;
            }
            const xX = [];
            for (let ii = 0; ii < data['xX'].length; ii++) {
                xX.push(data['xX'][ii].slice(5, 7) + '/' + data['xX'][ii].slice(8, 10));
            }
            this.stockDetail['MACD'] = data;
            this.chart[8] = {
                chart: { zoomType: 'x' },
                title: {
                    text: 'Moving Average Convergence/Divergence (MACD)',
                },
                subtitle: {
                    useHTML: true,
                    text: '<a style=\"text-decoration: none\" href=\"https://www.alphavantage.co/\" ' +
                    'target=\"_blank\">Source: Alpha Vantage</a>',
                },
                xAxis: [{
                    categories: xX.slice(data['index6months']),
                    tickInterval: 5,
                    labels: { style: { fontSize: '8px' } }
                }],
                yAxis: [{
                    title: {
                        text: 'MACD'
                    },
                }],
                legend: {
                    enabled: true,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                },
                exporting: { enabled: true },
                series: [{
                    type: 'line',
                    name: this.currentStock + ' MACD',
                    data: data['yY']['MACD'].slice(data['index6months']),
                }, {
                    type: 'line',
                    name: this.currentStock + ' MACD_Signal',
                    data: data['yY']['MACD_Signal'].slice(data['index6months']),
                }, {
                    type: 'line',
                    name: this.currentStock + ' MACD_Hist',
                    data: data['yY']['MACD_Hist'].slice(data['index6months']),
                }]
            };
        }, (err) => {
            // console.log('macd connect error');
            this.chart[8] = null;
        });
        this.http.get('/stock?symbol=' + this.currentStock + '&type=NEWS').subscribe(data => {
            if (data == null) {
                this.news = null;
                return;
            }
            this.news = data;
        }, (err) => {
            // console.log('news connect error');
            this.news = null;
        });
    }

    changeTabs1(tab1Selected) {
        this.tabs1 = tab1Selected;
    }

    changeTabs2(tabs2Selected) {
        this.tabs2 = tabs2Selected;
    }

    // favorite list
    addOrRemoveFav(favSymbol, ifAdd) {
        favSymbol = favSymbol.toUpperCase();
        if (ifAdd) {
            this.favList.push(new FavItem(this.favList.length + 1, this.currentStock, this.stockDetail['TIME']['lastPrice'],
                this.stockDetail['TIME']['change'], this.stockDetail['TIME']['changePercent'], this.stockDetail['TIME']['volume']));
            this.saveFavList();
        } else {
            for (let ii = 0; ii < this.favList.length; ii++) {
                if (this.favList[ii].symbol.toUpperCase() === favSymbol) {
                    this.favList.splice(ii, 1);
                    break;
                }
            }
            this.saveFavList();
        }
        this.isCurrentStockFavorite = false;
        for (let ii = 0; ii < this.favList.length; ii++) {
            if (this.favList[ii].symbol.toUpperCase() === this.currentStock.toUpperCase()) {
                this.isCurrentStockFavorite = true;
                break;
            }
        }
    }
    sortFavList(kind: string, ac: string) {
        switch (kind) {
            case '1':
                this.favList.sort(function(a, b) {return (a.order > b.order) ? 1 : ((b.order > a.order) ? -1 : 0); } );
                ac = '1';
                $('#order').val('1');
                break;
            case '2':
                this.favList.sort(function(a, b) {return (a.symbol > b.symbol) ? 1 : ((b.symbol > a.symbol) ? -1 : 0); } );
                break;
            case '3':
                this.favList.sort(function(a, b) {return (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0); } );
                break;
            case '4':
                this.favList.sort(function(a, b) {return (a.change > b.change) ? 1 : ((b.change > a.change) ? -1 : 0); } );
                break;
            case '5':
                this.favList.sort(function(a, b) {
                    return (a.changePercent > b.changePercent) ? 1 : ((b.changePercent > a.changePercent) ? -1 : 0); } );
                break;
            case '6':
                this.favList.sort(function(a, b) {return (a.volume > b.volume) ? 1 : ((b.volume > a.volume) ? -1 : 0); } );
                break;
            default:
        }
        switch (ac) {
            case '1':
                break;
            case '2':
                this.favList.reverse();
                break;
            default:
        }
        this.saveFavList();
    }
    refreshFavList() {
        // console.log('refreshing...');
        const tempFavList = this.favList.map(x => Object.assign({}, x));
        let finishedRefresh = 0;
        for (let ii = 0; ii < this.favList.length; ii++) {
            this.http.get('/stock?symbol=' + this.favList[ii].symbol + '&type=REFRESH').subscribe(data => {
                if (data == null) {
                    // console.log('refresh connection error');
                } else {
                    try {
                        // console.log('refreshing ' + this.favList[ii].symbol);
                        tempFavList[ii].price = data['lastPrice'];
                        tempFavList[ii].change = data['change'];
                        tempFavList[ii].changePercent = data['changePercent'];
                        tempFavList[ii].volume = data['volume'];
                        finishedRefresh = finishedRefresh + 1;
                        if (finishedRefresh === this.favList.length) {
                            this.favList = tempFavList;
                            this.saveFavList();
                            // console.log('refreshing done');
                        }
                    } catch (e) {
                        // console.log('refresh interupted');
                    }
                }
            });
        }
    }

    changeAutoRefreshState() {
        this.ifAutoRefresh = !(this.ifAutoRefresh);
        if (this.ifAutoRefresh) {
            // console.log('autofresh enable');
            this.autoRefreshObservable = Observable.timer(5000, 5000); // Call after 10 second.. Please set your time
            this.autoRefreshSubscription = this.autoRefreshObservable.subscribe(x => {
                this.refreshFavList();
            });
        }
        if (!(this.ifAutoRefresh)) {
            // console.log('autofresh disable');
            this.autoRefreshSubscription.unsubscribe();
        }
    }

    // facebook share
    saveInstance(ind: number, chartInstance) {
        this.chartInstance[ind] = chartInstance;
    }

    shareFB() {
        const charConfig = {
            options: JSON.stringify(this.chart[this.tabs2]),
            filename: 'asdf',
            type: 'image/png',
            async: true
        };

        // http://export.highcharts.com/
        const exportUrl = '/export';
        $.post(exportUrl, charConfig, function(data) {
            const url = 'http://export.highcharts.com/' + data;
            // console.log('exportPNG: ' + url);
            FB.ui({
                app_id: '129183671106069',
                // app_id:	'289313784895881',
                method:	'feed',
                picture: url,
            },	(response)	=>	{
                if	(response	&&	!response.error_message)	{
                    // succeed
                    alert('Posted Successfully');
                }	else	{
                    // fail
                    alert('Not Posted');
                }
            });
        });
    }

    setSymbol(theSym: string) {
        $('#stockSymbol').val(theSym);
        this.currentStock = theSym;
    }

    hisChartReflow() {
        // if (this.hisChart !== undefined && this.hisChart !== null ) {
            // $('#hisChartId').highcharts().reflow();
        // }
    }

    ifSortDefault() {
        return $('#sortby').val() === '1';
    }

    getResultAreaWidth() {
        return $('#formId').width() - 30;
    }

    getChartAreaWidth() {
        return $('#chartArea').width();
    }

    // favorite list and local storage manipulation
    saveFavList() {
        localStorage.setItem('zhiyFavList', JSON.stringify(this.favList));
    }
    getFavList() {
        const tempFavList = localStorage.getItem('zhiyFavList');
        if (tempFavList == null) {
            this.favList = [];
        } else {
            this.favList = JSON.parse(tempFavList);
        }
        this.sortFavList('1', '1');
    }
}

