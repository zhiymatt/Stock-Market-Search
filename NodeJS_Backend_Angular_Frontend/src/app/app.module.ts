import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { SlideComponent } from './slide/slide.component';

import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';

declare var require: any;
export function highchartsFactory() {
    const highcharts_mod = require('highcharts');
    const drilldown_mod = require('highcharts/modules/drilldown');
    const exporting_mod = require('highcharts/modules/exporting');
    const stock_mod = require('highcharts/modules/stock');

    drilldown_mod(highcharts_mod);
    exporting_mod(highcharts_mod);
    stock_mod(highcharts_mod);
    return highcharts_mod;
}

@NgModule({
    declarations: [
        AppComponent,
        SlideComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        HttpClientModule,
        ChartModule
    ],
    providers: [{ provide: HighchartsStatic, useFactory: highchartsFactory }],
    bootstrap: [AppComponent]
})
export class AppModule { }
