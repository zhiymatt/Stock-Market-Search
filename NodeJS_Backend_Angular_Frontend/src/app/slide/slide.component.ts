import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
    selector: 'app-slide',
    templateUrl: './slide.component.html',
    styleUrls: ['./slide.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('slide', [
            state('left', style({ transform: 'translateX(-50%)' })),
            state('right', style({ transform: 'translateX(0%)' })),
            transition('* => *', animate(300))
        ])
    ]
})
export class SlideComponent implements OnInit {
    @Input() activePane: PaneType;
    isRightVisible: boolean;

    constructor() {
    }

    ngOnInit() {
        this.isRightVisible = true;
    }
}

type PaneType = 'left' | 'right';
